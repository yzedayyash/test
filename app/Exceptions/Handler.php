<?php

namespace App\Exceptions;

use App\Models\Admin;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Access\AuthorizationException;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Arr;
class Handler extends ExceptionHandler
{
    protected $dontReport = [
        AuthenticationException::class,
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        TokenMismatchException::class,
        ValidationException::class,
    ];

    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    public function render($request, Exception $exception)
    {
        if ($exception instanceof UnauthorizedException) {

            if(Auth::user() instanceof Admin){
                return redirect()->route('admin.dashboard');
            }else if(Auth::user() instanceof Restaurant){
                return redirect()->route('restaurant.dashboard');
            }
            return redirect()->route('front');
        }

        return parent::render($request, $exception);
    }

    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'],401);
        }
        $guard = Arr::get($exception->guards(),0);

        if($guard == 'admin') {
            return redirect()->guest(route('admin.login_page'));
        }else  if($guard == 'restaurant') {
            return redirect()->guest(route('restaurant.login_page'));
        }

        return redirect()->guest(route('login_page'));
    }
}
