<?php
namespace App\Http\Controllers\Restaurant\Api;

use App\Http\Controllers\Controller;
use App\Models\Corporate\CorporateOrder;
use Carbon\Carbon;
use Illuminate\Http\Request;
use JWTAuth;

class DeliveryBoyController extends Controller
{
    public function getDeliveryRestaurants(Request $request)
    {

        $user = JWTAuth::user();
        $assigned_orders = CorporateOrder::
            leftJoin('corporates', 'corporates.id', '=', 'corporate_orders.corporate_id')
            ->leftJoin('restaurant_stores', 'restaurant_stores.id', '=', 'corporate_orders.delivery_boy')

            ->where('corporate_order_status_id', 3)
            ->where('restaurant_store_id', $user->restaurant_store_id)
            ->where('delivery_boy', $user->id)
            ->select('corporate_orders.corporate_id',
                'corporates.company_name',
                'corporate_orders.delivery_time_from',
                'corporate_orders.delivery_time_to',
                'corporate_orders.order_date',
                'corporates.address',
                'corporates.longitude as corporate_long',
                'corporates.latitude as corporate_lat',
                'restaurant_stores.longitude as restaurant_stores_long',
                'restaurant_stores.latitude as restaurant_stores_lat',
                'restaurant_stores.slug as restaurant_stores_name',

            )
            ->groupBy('corporate_orders.corporate_id')->get();

        $assigned_orders = $assigned_orders->map(function ($order) use ($user) {
            return [
                'corporate_id' => $order->corporate_id,
                'corporate_name' => $order->company_name,
                'delivery' => $order->delivery_time_from . ' - ' . $order->delivery_time_to,
                'order_date' => Carbon::parse($order->order_date)->format('D d-m-Y'),
                'corporate_address' => $order->address,
                'corporate_long' => $order->corporate_long,
                'corporate_lat' => $order->corporate_lat,
                'restaurant_stores_lat' => $order->restaurant_stores_lat,
                'restaurant_stores_long' => $order->restaurant_stores_long,
                'restaurant_stores_name' => $order->restaurant_stores_name,
                'note' => '',
                'restaurant_store_id' => $user->restaurant_store_id,
            ];
        }
        );
        return response()->json(['data' => $assigned_orders], 200);
    }


    public function changeStatus(Request $request)
    {
        $date  = Carbon::parse($request->order_date)->format('Y-m-d');
        $user = JWTAuth::user();
        $corporate_id = $request->corporate_id;

     $request->status == 1 ?   $status = 4 : $status = 5;

        CorporateOrder::where('corporate_id' , $corporate_id)->where('delivery_boy' , $user->id)->where('order_date' , $date)->update([
            'corporate_order_status_id' => $status
        ]);

        return response()->json(['data' => 'success'], 200);





    }
}
