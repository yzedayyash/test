<?php

namespace App\Http\Controllers\Restaurant;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Carbon\Carbon;
use Auth;
use App\Models\Corporate\CorporateStoreByDay;
use App\Models\Corporate\Corporate;
use App\Models\Corporate\CorporateOrder;


use App\Models\Item\Category;
use App\Models\Corporate\CorporateOrderItem;
use App\Models\Corporate\CorporateCategorySetting;

class CalenderController extends Controller
{
     public function index(Request $request){
        $data = array();

        //get days
        Carbon::setWeekStartsAt(Carbon::SATURDAY);
        Carbon::setWeekEndsAt(Carbon::FRIDAY);

        for ($i = 0; $i < 7; $i++) {
            $day[] = Carbon::now()->startOfWeek()->addDays($i)->format('Y-m-d');
        }
        $all_days = collect($day);
        if($request->selected_day) {
            $selected_day = $request->selected_day;
        }else {
            $selected_day = Carbon::now()->format('Y-m-d');

        }

        $day_format = Carbon::parse($selected_day)->format('D');

        $corporates = CorporateStoreByDay::where('day' , $day_format)->where('restaurant_store_id' , Auth::guard('restaurant')->user()->restaurant_store_id )
        ->with('corporate')
        ->get();


        $categories =Category::with(["category_translation" => function ($q) {
            $q->where('language_id', '=', 1);
        }])
            ->get();

        $corporates = $corporates->map(function ($corporate) use ($categories , $selected_day) {
            $categroy_array = array();
            // dd($corporate->corporate);
            foreach($categories as $category) {


                $data['delivery'] = CorporateCategorySetting::where('corporate_id' ,$corporate->corporate->id )
                ->where('category_id' ,$category->id)->first();

            $data['name'] = $category->category_translation[0]->name;
                array_push($categroy_array , $data);
            }

                $orders = array();

            // dd($categroy_array);
            return [
                'id' => $corporate->corporate->id,

                'company_name' => $corporate->corporate->company_name,
                'address' => $corporate->corporate->address,
                'categories' => $categroy_array,
                'orders' => $categroy_array,

            ];

    });


        return view('restaurant.pages.calender.list')->with([
            'days' => $all_days,
            'selected_day' => $selected_day,
            'corporates' => $corporates,
            // 'categories' => $categories,
        ]);
     }
}
