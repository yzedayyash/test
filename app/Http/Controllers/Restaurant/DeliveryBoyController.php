<?php
namespace App\Http\Controllers\Restaurant;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Carbon\Carbon;
use Auth;
use Hash;

use App\Models\Corporate\CorporateStoreByDay;
use App\Models\Corporate\Corporate;
use App\Models\Corporate\CorporateOrder;
use App\Models\Restaurant\RestaurantStore;
use App\Models\Restaurant\RestaurantUser;

use App\Models\Item\Item;

use App\Models\Item\Category;
use App\Models\Corporate\CorporateOrderItem;
use App\Models\Corporate\CorporateCategorySetting;



class DeliveryBoyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:restaurant');


    }

    public function index(Request $request)
    {
        $data = array();

        //get days
        Carbon::setWeekStartsAt(Carbon::SATURDAY);
        Carbon::setWeekEndsAt(Carbon::FRIDAY);

        for ($i = 0; $i < 7; $i++) {
            $day[] = Carbon::now()->startOfWeek()->addDays($i)->format('Y-m-d');
        }
        $all_days = collect($day);
        if($request->selected_day) {
            $selected_day = $request->selected_day;
        }else {
            $selected_day = Carbon::now()->format('Y-m-d');

        }

        $day_format = Carbon::parse($selected_day)->format('D');

        $corporates = CorporateStoreByDay::where('day' , $day_format)->where('restaurant_store_id' , Auth::guard('restaurant')->user()->restaurant_store_id )
        ->with('corporate')
        ->get();

         $categories =Category::with(["category_translation" => function ($q) {
            $q->where('language_id', '=', 1);
        }])
            ->get();




            $corporates = $corporates->map(function ($corporate) use ($categories , $selected_day) {
                $categroy_array = array();
                // dd($corporate->corporate);
                foreach($categories as $category) {


                $data['count'] = CorporateOrder::where('category_id', '=', $category->id)  ->where('corporate_order_status_id' , 2)
                       ->whereHas('corporate_user', function ($query) use ($category , $corporate) {
                        $query->where('corporate_id', '=', $corporate->corporate->id);
                    })->where('order_date', $selected_day)->count();


                    $data['delivery'] = CorporateCategorySetting::where('corporate_id' ,$corporate->corporate->id )
                    ->where('category_id' ,$category->id)->first();
                $data['name'] = $category->category_translation[0]->name;
                    array_push($categroy_array , $data);
                }


                    $orders = array();

                return [
                    'id' => $corporate->corporate->id,
                    'company_name' => $corporate->corporate->company_name,
                    'address' => $corporate->corporate->address,
                    'categories' => $categroy_array,
                    'orders' => $categroy_array,

                ];

        });

        $delivery_users = RestaurantUser::where('restaurant_store_id' , Auth::guard('restaurant')->user()
        ->restaurant_store_id)->whereHas('roles', function ($query) {
            return $query->where('name', 'delivery_boy');
        })->get();

        $assigned_orders = CorporateOrder::
        leftJoin('corporate_users' , 'corporate_users.id' , '=' , 'corporate_orders.corporate_user_id')
        ->leftJoin('corporates' , 'corporates.id' , '=' , 'corporate_users.corporate_id')
        ->where('corporate_order_status_id' , 3)
        ->where('restaurant_store_id' , Auth::guard('restaurant')->user()->restaurant_store_id)
        ->with('delivery_boy_data')
        ->where('order_date' , $selected_day)
        ->with('corporate_order_status_history')->groupBy('corporate_users.corporate_id' , 'delivery_boy' )->get();


        return view('restaurant.pages.delivery_boy.list')->with([
            'days' => $all_days,
            'selected_day' => $selected_day,
            'corporates' => $corporates,
            'categories' => $categories,
            'delivery_users' => $delivery_users,
            'assigned_orders' => $assigned_orders,
            'delivered_orders' => $delivery_users,

        ]);

    }

    public function insert(Request $request){

        $this->validate($request, [
            'email' => 'required|email|unique:restaurant_users',
            'name' => 'required|string|max:50',
            'password' => 'required',

        ]);
    $store_id =   Auth::guard('restaurant')->user()->restaurant_store_id;

        $delivery_boy = new RestaurantUser();
        $delivery_boy->name = $request->name;
        $delivery_boy->email = $request->email;
        $delivery_boy->password = Hash::make($request->password);
        $delivery_boy->status = 1;
        $delivery_boy->restaurant_store_id =  $store_id ;
        $delivery_boy->syncRoles(['delivery_boy']);
        $delivery_boy->save();
        return redirect()->back();

    }



    public function assignOrder(Request $request){

        if($request->selected_day) {
            $selected_day = $request->selected_day;
        }else {
            $selected_day = Carbon::now()->format('Y-m-d');
        }

        $assigned_orders = CorporateOrder::
        leftJoin('corporate_users' , 'corporate_users.id' , '=' , 'corporate_orders.corporate_user_id')
        ->where('order_date', $selected_day)
        ->where('corporate_order_status_id' , 2)
        ->where('corporate_users.corporate_id' , $request->corporate_id)
        ->where('restaurant_store_id' , Auth::guard('restaurant')->user()->restaurant_store_id)
        ->update(['corporate_order_status_id' => 3 ,
                'delivery_boy' => $request->delivery
        ] );
        return redirect()->back();
    }

}
