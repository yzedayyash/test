<?php

namespace App\Http\Controllers\Restaurant;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\Item\Item;

use App\Models\Item\Category;

class MenuController extends Controller
{
    public function index(){
        $rest_id = Auth::guard('restaurant')->user()->restaurant_store_id;

      $items =  Category::with(['item' => function($q) use ($rest_id){
            $q->where('restaurant_store_id'  , $rest_id);
        }])->get();

        return view('restaurant.pages.menu.list')->with([
            'data' => $items,

            // 'categories' => $categories,
        ]);
    }


    public function deactiveItemAjax(Request $request){
        if($request->ajax()){
            Item::where('id' , $request->item_id)->update([
                'status' => $request->checked
            ]);
            return 'success';
        }
       abort(400);
    }

    public function updateItemQuantityAjax(Request $request){
        if($request->ajax()){
            Item::where('id' , $request->item_id)->update([
                'quantity_per_day' => $request->quantity
            ]);
            return  'success';
        }
       abort(400);
    }
}
