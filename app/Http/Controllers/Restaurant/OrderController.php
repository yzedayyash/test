<?php

namespace App\Http\Controllers\Restaurant;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Carbon\Carbon;
use Auth;
use App\Models\Corporate\CorporateStoreByDay;
use App\Models\Corporate\Corporate;
use App\Models\Corporate\CorporateOrder;
use App\Models\Restaurant\RestaurantStore;

use App\Models\Item\Item;

use App\Models\Item\Category;
use App\Models\Corporate\CorporateOrderItem;
use App\Models\Corporate\CorporateCategorySetting;



class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:restaurant');


    }

    public function index(Request $request)
    {
        $data = array();

        //get days
        Carbon::setWeekStartsAt(Carbon::SATURDAY);
        Carbon::setWeekEndsAt(Carbon::FRIDAY);

        for ($i = 0; $i < 7; $i++) {
            $day[] = Carbon::now()->startOfWeek()->addDays($i)->format('Y-m-d');
        }
        $all_days = collect($day);
        if($request->selected_day) {
            $selected_day = $request->selected_day;
        }else {
            $selected_day = Carbon::now()->format('Y-m-d');

        }

        $day_format = Carbon::parse($selected_day)->format('D');

        $corporates = CorporateStoreByDay::where('day' , $day_format)->where('restaurant_store_id' , Auth::guard('restaurant')->user()->restaurant_store_id )
        ->with('corporate')
        ->get();

         $categories =Category::with(["category_translation" => function ($q) {
            $q->where('language_id', '=', 1);
        }])
            ->get();



            $corporates = $corporates->map(function ($corporate) use ($categories , $selected_day) {
                $categroy_array = array();

                foreach($categories as $category) {

                $data['count'] = CorporateOrder::where('category_id', $category->id)
                       ->whereHas('corporate_user', function ($query) use ($category , $corporate) {
                        $query->where('corporate_id', '=', $corporate->corporate->id);
                    })->where('order_date', $selected_day)->where('corporate_order_status_id' , 1)->count();


                    $data['delivery'] = CorporateCategorySetting::where('corporate_id' ,$corporate->corporate->id )
                    ->where('category_id' ,$category->id)->first();
                $data['name'] = $category->category_translation[0]->name;
                    array_push($categroy_array , $data);
                }

                    $orders = array();

                // dd($categroy_array);
                return [
                    'id' => $corporate->corporate->id,

                    'company_name' => $corporate->corporate->company_name,
                    'address' => $corporate->corporate->address,
                    'categories' => $categroy_array,
                    'orders' => $categroy_array,

                ];

        });


        return view('restaurant.pages.order.list')->with([
            'days' => $all_days,
            'selected_day' => $selected_day,
            'corporates' => $corporates,
            'categories' => $categories,
        ]);

    }



   public function getCorporateOrderAjax(Request $request){
        if($request->ajax()){
            $corporate_id = $request->corporate_id;
            $date = $request->date;

               $corporate_categories = Category::whereHas('corporate_setting', function ($q) use ($corporate_id) {
            $q->where('corporate_id', '=', $corporate_id);
        })->with(["category_translation" => function ($q) {
            $q->where('language_id', '=', 1);
        }])
            ->get();
            foreach($corporate_categories as $category){

                 $category->item = Item::where('category_id' , $category->id)
                 ->where('restaurant_store_id' , Auth::guard('restaurant')->user()->restaurant_store_id)
                  ->with(['corporate_order_item' => function ($q) use ($date , $corporate_id){

                    $q->whereHas('corporate_order' , function ($q) use ( $date , $corporate_id)
                    {
                        $q->whereHas('corporate_user', function ($q) use ($corporate_id) {
                            $q->where('corporate_id', '=', $corporate_id);
                        })->where('order_date', $date)->where('corporate_order_status_id' , 1);
                    })->with(['corporate_order.corporate_user'] );
                  }])->with(["item_translation" => function ($q) {
                    $q->where('language_id', '=', 1);
                }])




                  ->get();
            }
            $corporate_categories = $corporate_categories->map(function ($category) use ($corporate_id ) {
                return [
                    'corporate_id' => $corporate_id,

                    'category' => $category->category_translation[0]->name,
                    'category_id' => $category->id,

                    'items' => $category->item->map(function ($item) {
                        return [
                            'slug' => $item->slug,

                            'name' => $item->item_translation[0]->name,
                            'thumb' => $item->thumb,
                            'details' => $item->corporate_order_item->map(function ($ite) {
                                return [
                                    'quantity' => $ite->quantity,
                                    'order_id' => $ite->corporate_order_id,
                                    'note' => $ite->note,
                                    'user' => $ite->corporate_order->corporate_user->name,
                                ];
                            }),

                        ];
                    }),
                ];
            });
                return $corporate_categories;
        }
       abort(400);

   }

   public function dispatchOrder($date , $corporate_id , $category_id){
       $restaurant_id = Auth::guard('restaurant')->user()->restaurant_store_id;
       $data = CorporateOrder::where('category_id' , $category_id)->where('order_date' , $date)->whereHas('corporate_user', function ($q) use ($corporate_id) {
            $q->where('corporate_id', '=', $corporate_id);
        })->where('restaurant_store_id' , $restaurant_id  )
        ->update([
            'corporate_order_status_id'=>2,
        ]);

            return redirect()->back();
}


public function printOrder($date , $corporate_id , $category_id){
    $restaurant_id = Auth::guard('restaurant')->user()->restaurant_store_id;
    $data = CorporateOrder::where('category_id' , $category_id)
    ->with('corporate_order_item.item')
    ->with('corporate_user')
    ->where('order_date' , $date)->whereHas('corporate_user', function ($q) use ($corporate_id) {
         $q->where('corporate_id', '=', $corporate_id);
     })->where('restaurant_store_id' , $restaurant_id  )->get();


     $restaurant_details = RestaurantStore::where('id' , $restaurant_id)->first();

     $corporate_details = Corporate::where('id' , $restaurant_id)->with(['corporate_setting' => function ($q) use ($category_id){
            $q->where('category_id' , $category_id);
     }])->first();

     return view('restaurant.pages.order.print')->with([
        'date' => $date,
        'results' => $data,
        'restaurant' => $restaurant_details,
        'corporate' => $corporate_details

    ]);
        //  return redirect()->back();
}

}
