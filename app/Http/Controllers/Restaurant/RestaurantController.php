<?php

namespace App\Http\Controllers\Restaurant;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;

class RestaurantController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:restaurant');

     
    }

    public function index()
    {
        return view('restaurant.pages.dashboard'); 
    }

    public function index2()
    {
        return view('restaurant.pages.dashboard2'); 
    }
}