<?php

namespace App\Http\Controllers\Restaurant;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\Restaurant\RestaurantStoreRate;


class ReviewController extends Controller
{
    public function index(){
        $rest_id = Auth::guard('restaurant')->user()->restaurant_store_id;

      $reviews =  RestaurantStoreRate::with('corporate_user')->where('restaurant_store_id' , $rest_id)->get();

        return view('restaurant.pages.review.list')->with([
            'data' => $reviews,

            // 'categories' => $categories,
        ]);
    }
}
