<?php

namespace App\Http\Controllers\Restaurant;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\Restaurant\RestaurantUser;

class SettingController extends Controller
{
    public function index(){
        $user = Auth::guard('restaurant')->user();

        return view('restaurant.pages.setting.edit')->with([
            'data' => $user,
        ]);
    }


    public function update(Request $request){
        $user = Auth::guard('restaurant')->user();
        $rest = RestaurantUser::find($user->id);
        $rest->email = $request->email;
        $rest->name = $request->name;
$rest->save();

        return redirect()->back();

    }
}
