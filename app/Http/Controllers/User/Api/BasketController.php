<?php

namespace App\Http\Controllers\User\Api;

use App\Http\Controllers\Controller;
use App\Models\Corporate\CorporateBasket;
use App\Models\Item\Item;
use Carbon\Carbon;
use Illuminate\Http\Request;
use JWTAuth;
use App\Traits\SettingTrait;

class BasketController extends Controller
{
    use SettingTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $user = JWTAuth::user();
        $language_id = $request->language_id;
        $price_unit = $this->getSetting('price_unit', $language_id);

        if ($user->hasRole('employee')) {
            $user_basket = CorporateBasket::with('items')
                ->with(["items.item_translation" => function ($q) use ($language_id) {
                    $q->where('language_id', '=', $language_id);
                }])->with(["items.item_unit.item_unit_translation" => function ($q) use ($language_id) {
                $q->where('language_id', '=', $language_id);
            }])
                ->with(["items.item_specification.item_specification_translation" => function ($q) use ($language_id) {
                    $q->where('language_id', '=', $language_id);
                }])
                ->with(["items.restaurant_store.restaurant_store_translation" => function ($q) use ($language_id) {
                    $q->where('language_id', '=', $language_id);
                }])
                ->with('items.item_image')
                ->where('corporate_user_id', $user->id)->get();

            $total = 0;
            $user_basket = $user_basket->map(function ($basket) use (&$total , $price_unit) {
                $total += $basket->items->price * $basket->quantity;
                return [
                    'id' => $basket->id,
                    'note' => $basket->note,
                    'quantity' => $basket->quantity,
                    'item' => collect([
                        'id' => $basket->items->id,
                        'old_price' => $basket->items->old_price,
                        'price' => $basket->items->price,
                        'quantity' => $basket->items->quantity_per_day,
                        'thumb' =>  $basket->items->thumb,
                        'price_unit' => $price_unit,
                        'name' => $basket->items->item_translation[0]->name,
                        'description' => $basket->items->item_translation[0]->description,
                        'unit' => $basket->items->item_unit->item_unit_translation[0]->name,
                        'store_name' => $basket->items->restaurant_store->restaurant_store_translation[0]->name,
                        'item_total' => $basket->items->price * $basket->quantity,
                        'specifications' => $basket->items->item_specification->map(function ($spec) {
                            return [
                                'name' => $spec->item_specification_translation[0]->name,
                            ];

                        }),

                        'item_images' => $basket->items->item_image->map(function ($image){
                            return [
                                'image' => $image->image
                            ];
                         }),
                    ]),

                ];

            });
            if(count($user_basket) > 0){
                $store =   $user_basket[0]['item']['store_name'];
            }else {
                $store = '';
            }


            return response()->json(['data' => $user_basket, 'total' => $total , 'price_unit' => $price_unit , 'store_name' => $store ], 200);
        }
        return response()->json(['error' => 'invalid_role'], 401);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = JWTAuth::user();

        if ($user->hasRole('employee')) {
            $data = array();
            $this->validate($request, [
                'item_id' => 'required',
                'quantity' => 'required',
            ]);

            $item = Item::where('id' , $request->item_id)->first();

          $basket =   CorporateBasket::where('corporate_user_id' , $user->id)->get();

          if(!count($basket) == 0) {
              foreach ($basket as $key => $value) {
                 if($value->category_id != $item->category_id) {
                    return response()->json(['error' => ' not same category'], 400);

                 }

                 if($value->restaurant_store_id != $item->restaurant_store_id) {
                    return response()->json(['error' => ' not same store'], 400);

                 }

                 if($value->date != Carbon::parse($request->date)->format('Y-m-d')) {
                    return response()->json(['error' => ' not same date'], 400);

                 }
              }
          }
            $basket = new CorporateBasket();
            $basket->item_id = $request->item_id;
            $basket->quantity = $request->quantity;
            $basket->note = $request->note;
            $basket->date = Carbon::parse($request->date)->format('Y-m-d');
            $basket->category_id = $item->category_id;
            $basket->restaurant_store_id = $item->restaurant_store_id;

            $basket->corporate_user_id = $user->id;
            if ($basket->save()) {
                return response()->json(['data' => 'success'], 201);

            }
            return response()->json(['error' => 'something went wrong'], 400);

        }
        return response()->json(['error' => 'invalid_role'], 401);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Corporate\CorporateBasket  $corporateBasket
     * @return \Illuminate\Http\Response
     */
    public function show(CorporateBasket $corporateBasket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Corporate\CorporateBasket  $corporateBasket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CorporateBasket $corporateBasket)
    {
        $user = JWTAuth::user();

        if ($user->hasRole('employee')) {

            $language_id = $request->language_id;
            $corporateBasket->note = $request->note;

            $corporateBasket->quantity = $request->quantity;

            $corporateBasket->save();

            $user_basket = CorporateBasket::with('items')->where('corporate_user_id', $user->id)->get();

            $total = 0 ;
            $user_basket = $user_basket->map(function ($basket) use (&$total) {
                $total += $basket->items->price * $basket->quantity;

                return $total;
            });
            $price_unit = $this->getSetting('price_unit', $language_id);

            return response()->json(['data' => 'success' , 'total' => $total , 'price_unit' => $price_unit], 200);
        }
        return response()->json(['error' => 'invalid_role'], 401);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Corporate\CorporateBasket  $corporateBasket
     * @return \Illuminate\Http\Response
     */
    public function destroy(CorporateBasket $corporateBasket , Request $request)
    {
        $user = JWTAuth::user();

        if ($user->hasRole('employee')) {
            $corporateBasket->where('id' , $request->id)->delete();
            return response()->json(['data' => 'success'], 200);
        }
        return response()->json(['error' => 'invalid_role'], 401);
    }
}
