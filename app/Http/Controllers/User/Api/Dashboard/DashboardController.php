<?php

namespace App\Http\Controllers\User\Api\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Corporate\Corporate;
use App\Models\Corporate\CorporateOrder;
use App\Models\Corporate\CorporateOrderItem;
use App\Models\Restaurant\RestaurantStoreRate;
use JWTAuth;

class DashboardController extends Controller
{
    public function index()
    {
        $user = JWTAuth::parseToken()->authenticate();

        if ($user->hasRole('manager')) {
            $corporate_stores = Corporate::with('corporate_store_by_day')->get();
            return $corporate_stores;
            $corporate_store = $corporate_stores->map(function ($corporate_store) {
                return [
                    'code' => $corporate_store->code,
                    'company_name' => $corporate_store->company_name,
                    'address' => $corporate_store->address,
                    'corporate_stores' => $corporate_store->corporate_store_by_day->map(function ($store) {
                        return [
                            'id' => $store->id,
                            'logo' => $store->logo,
                            'phone' => $store->phone,
                            'email' => $store->email,
                            'adderss' => $store->adderss,
                            'count' => CorporateOrderItem::whereHas('item', function ($query) use ($store) {
                                $query->where('restaurant_store_id', '=', $store->id);
                            })->count(),

                            'rate' => RestaurantStoreRate::where('restaurant_store_id', $store->id)->avg('rate'),

                        ];
                    }),
                ];
            });

            $top_employees = CorporateOrder::whereHas('corporate_user.corporate', function ($query) use ($user) {
                $query->where('id', '=', $user->corporate_id);
            })->with('corporate_user')->select('corporate_user_id', \DB::raw('count(*) as total'))
                ->groupBy('corporate_user_id')
                ->limit(3)->get();

            $top_employees = $top_employees->map(function ($top_employees) {
                return [
                    'corporate_user_id' => $top_employees->corporate_user_id,
                    'total' => $top_employees->total,
                    'name' => $top_employees->corporate_user->name,
                ];
            });

            $data = array();
            $data['corporate_store'] = $corporate_store;
            $data['top_employees'] = $top_employees;
            return response()->json(['data' => $data], 200);
        }

        return response()->json(['error' => 'invalid_role'], 401);
    }

}
