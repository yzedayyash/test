<?php

namespace App\Http\Controllers\User\Api\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Corporate\CorporateDepartment;
use App\Models\Corporate\CorporateUser;
use Illuminate\Http\Request;
use JWTAuth;

class DepartmentController extends Controller
{
    public function index()
    {
        $user = JWTAuth::parseToken()->authenticate();

        if ($user->hasRole('manager')) {
            $corporate_departments = CorporateDepartment::with('corporate_department_translation')
           ->with('sub_departments')
            ->withCount('department_user')
            ->where('corporate_id', $user->corporate_id)
            ->where('parent_id' , null)->paginate(10);

            $corporate_departments->getCollection()->transform(function ($corporate_department)  {

                return [
                    'id' => $corporate_department->id,
                    'name' => $corporate_department->corporate_department_translation[0]->name,
                    'count' => $corporate_department->department_user->count(),
                    'sub_departments' => $corporate_department->sub_departments,
                ];
            });

            return response()->json(['data' => $corporate_departments], 200);
        }

        return response()->json(['error' => 'invalid_role'], 401);
    }

    public function departmetUsers($id)
    {
        $user = JWTAuth::parseToken()->authenticate();

        if ($user->hasRole('manager')) {
            $department_users = CorporateUser::whereHas('corporate_department', function ($query) use ($id) {
                $query->where('corporate_department_id', '=', $id);
            })->paginate(10);
            $department_users->getCollection()->transform(function ($department_user) {
                return [
                    'id' => $department_user->id,
                    'name' => $department_user->name,
                ];
            });
            return response()->json(['data' => $department_users], 200);
        }

        return response()->json(['error' => 'invalid_role'], 401);
    }

    public function addDepartment(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        if ($user->hasRole('manager')) {
            $this->validate($request, [
                'name' => 'required',
                'status' => 'required',
            ]);

            $corporate_department = new CorporateDepartment();
            $corporate_department->parent_id = $request->parent_id;
            $corporate_department->corporate_id = $user->corporate_id;
            $corporate_department->status = $request->status;
            $corporate_department->save();

            foreach ($request->languages as $key => $lang) {
                $corporate_department_translation = new CorporateDepartmentTranslation();
                $corporate_department_translation->language_id = $lang;
                $corporate_department_translation->name = $request->name;
                $corporate_department_translation->corporate_department_id = $corporate_department->id;
                $corporate_department_translation->save();
            }

            return response()->json(['data' => 'success'], 200);

        }

        return response()->json(['error' => 'invalid_role'], 401);
    }

    public function editDepartment($id)
    {
        $user = JWTAuth::parseToken()->authenticate();

        if ($user->hasRole('manager')) {

            $corporate_department = CorporateDepartment::with('corporate_department_translation')->where('corporate_id', $user->corporate_id)->where('id', $id)->first();

            $corporate_department_array = collect([
                'id' => $corporate_department->id,
                'translation' => $corporate_department->corporate_department_translation,
                'status' => $corporate_department->status,

            ]);

            return response()->json(['data' => $corporate_department_array], 200);
        }

        return response()->json(['error' => 'invalid_role'], 401);
    }

    public function updateDepartment(Request $request, $id)
    {
        $user = JWTAuth::parseToken()->authenticate();

        if ($user->hasRole('manager')) {

            $this->validate($request, [
                'name' => 'required',
                'status' => 'required',

            ]);
            $corporate_user = CorporateDepartment::find($id);
            $corporate_user->name = $request->name;
            $corporate_user->status = $request->status;
            $corporate_user->modified_at = time();
            $corporate_user->modified_by = $user->id;

            return response()->json(['data' => 'updated successfully '], 200);
        }

        return response()->json(['error' => 'invalid_role'], 401);
    }

}
