<?php

namespace App\Http\Controllers\User\Api\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Corporate\CorporateUser;
use Illuminate\Http\Request;
use JWTAuth;
use App\Traits\NotificationTrait;
use  Illuminate\Support\Str;

use App\Models\Corporate\InviteCorporateUser;

class EmployeeController extends Controller
{    use NotificationTrait;


    public function employees(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        if ($user->hasRole('manager')) {
            $corporate_user = CorporateUser::role('employee')->where('corporate_id', $user->corporate_id);
            if ($request->name) {
                $corporate_user = $corporate_user->where('name', 'like', '%' . $request->name . '%');
            }
            if ($request->email) {
                $corporate_user = $corporate_user->where('email', 'like', '%' . $request->email . '%');
            }
            if ($request->mobile) {
                $corporate_user = $corporate_user->where('mobile', 'like', '%' . $request->mobile . '%');
            }
            $corporate_users = $corporate_user->paginate(10);

            $corporate_users->getCollection()->transform(function ($corporate_user) {
                return [
                    'id' => $corporate_user->id,
                    'name' => $corporate_user->name,
                    'job_title' => $corporate_user->job_title,
                    'mobile' => $corporate_user->mobile,
                    'email' => $corporate_user->email,
                    'status' => $corporate_user->status,

                ];
            });

            return response()->json(['data' => $corporate_users], 200);
        }
        return response()->json(['error' => 'invalid_role'], 401);
    }

    public function blockEmployee($id)
    {
        $user = JWTAuth::parseToken()->authenticate();
        if ($user->hasRole('manager')) {
            $corporate_user = CorporateUser::role('employee')->find($id);

            $corporate_user->status = 0;
            $corporate_user->modified_at = time();
            $corporate_user->modified_by = $user->id;
            $corporate_user->save();

            return response()->json(['data' => 'success'], 200);

        }
        return response()->json(['error' => 'invalid_role'], 401);
    }

    public function deleteEmployee($id)
    {
        $user = JWTAuth::parseToken()->authenticate();
        if ($user->hasRole('manager')) {
            $corporate_user = CorporateUser::role('employee')->find($id);

            $corporate_user->deleted = 1;
            $corporate_user->deleted_by = time();
            $corporate_user->deleted_at = $user->id;
            $corporate_user->save();

            return response()->json(['data' => 'success'], 200);

        }
        return response()->json(['error' => 'invalid_role'], 401);
    }

    public function inviteEmployee(Request $request){

        $user = JWTAuth::parseToken()->authenticate();
        if ($user->hasRole('manager')) {

            $email = $request->email;
            $token = Str::random(60);
            $url = route('employee.invited', ['token' => $token]);
           $corporate =  $user->corporate->code;

           $invited_user = new InviteCorporateUser();
           $invited_user->email = $request->email;
           $invited_user->token = $token;
           $invited_user->corporate_code = $corporate;

           $invited_user->save();
            $details = [

                'greeting' => 'Hi '.$email,

                'body' => 'You Have Been Invited To Use test@employee\n ',

                'thanks' => 'Thank you for using test',

                'actionText' => 'Complete Registration',

                'actionURL' =>  $url,


            ];
            $this->InvitiationEmail($details,$invited_user);

            return response()->json(['data' => 'success'], 200);

        }
        return response()->json(['error' => 'invalid_role'], 401);



    }

}
