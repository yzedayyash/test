<?php

namespace App\Http\Controllers\User\Api\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Restaurant\RestaurantStore;
use Illuminate\Http\Request;
use JWTAuth;

class RestaurantController extends Controller
{

    public function restaurants(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        if ($user->hasRole('manager')) {
            $restaurant_stores = RestaurantStore::paginate(10);

            $restaurant_stores->getCollection()->transform(function ($restaurant_store) {
                return [
                    'id' => $restaurant_store->id,
                    'logo' => $restaurant_store->logo,
                    'name' => $restaurant_store->slug,
                ];
            });

            return response()->json(['data' => $restaurant_stores], 200);
        }
        return response()->json(['error' => 'invalid_role'], 401);
    }
}
