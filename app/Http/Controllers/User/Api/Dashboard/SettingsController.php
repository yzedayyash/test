<?php

namespace App\Http\Controllers\User\Api\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Item\Category;
use Illuminate\Http\Request;
use JWTAuth;

class SettingsController extends Controller
{
    public function index(Request $request)
    {

        $user = JWTAuth::parseToken()->authenticate();

        if ($user->hasRole('manager')) {
            $this->validate($request, [
                'language_id' => 'required',

            ]);
            $language = $request->language_id;
            $corporate_stores = Category::with(["corporate_setting" => function ($q) {
                $q->where('corporate_id', '=', 1);
            }])->with(["category_translation" => function ($q) use ($language) {
                $q->where('language_id', '=', $language);
            }])
                ->get();

            $corporate_store = $corporate_stores->map(function ($corporate_store) {
                return [
                    'id' => $corporate_store->id,
                    'name' => $corporate_store->category_translation[0]->name,
                    'settings' => $corporate_store->corporate_setting->map(function ($settings) {
                        return [
                            'order_time_from' => $settings->order_time_from,
                            'order_time_to' => $settings->order_time_to,
                            'delivery_time_from' => $settings->delivery_time_from,
                            'delivery_time_to' => $settings->delivery_time_to,
                        ];
                    }),
                ];
            });

            $data = array();
            $data['corporate_store'] = $corporate_store;
            return response()->json(['data' => $data], 200);
        }

        return response()->json(['error' => 'invalid_role'], 401);
    }

}
