<?php

namespace App\Http\Controllers\User\Api\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Corporate\CorporateUser;
use Illuminate\Http\Request;
use JWTAuth;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{

    public function users(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        if ($user->hasRole('manager')) {
            $corporate_user = CorporateUser::whereHas('roles', function ($query) {
                return $query->where('name', '!=', 'employee');
            })
                ->with('roles')
                ->where('corporate_id', $user->corporate_id);
            if ($request->name) {
                $corporate_user = $corporate_user->where('name', 'like', '%' . $request->name . '%');
            }

            if ($request->email) {
                $corporate_user = $corporate_user->where('email', 'like', '%' . $request->mobile . '%');
            }
            $corporate_users = $corporate_user->paginate(10);

            $corporate_users->getCollection()->transform(function ($corporate_user) {
                return [
                    'id' => $corporate_user->id,
                    'name' => $corporate_user->name,
                    'job_title' => $corporate_user->job_title,
                    'mobile' => $corporate_user->mobile,
                    'email' => $corporate_user->email,
                    'status' => $corporate_user->status,
                    'roles' => $corporate_user->roles->map(function ($role) {
                        return [
                            'roles' => $role->name,
                        ];
                    }),
                ];
            });

            return response()->json(['data' => $corporate_users], 200);
        }
        return response()->json(['error' => 'invalid_role'], 401);
    }

    public function editUser(Request $request, $id)
    {
        $user = JWTAuth::parseToken()->authenticate();
        if ($user->hasRole('manager')) {

            $corporate_user = CorporateUser::with('roles')
                ->where('corporate_id', $user->corporate_id)
                ->where('id', $id)->first();

            $corporate_user_array = collect([
                'id' => $corporate_user->id,
                'name' => $corporate_user->name,
                'job_title' => $corporate_user->job_title,
                'mobile' => $corporate_user->mobile,
                'email' => $corporate_user->email,
                'status' => $corporate_user->status,
                'roles' => $corporate_user->roles->map(function ($role) {
                    return [
                        'roles' => $role->name,
                    ];
                }),
            ]);

            $roles = Role::where('guard_name', 'web')->get('name');
            $data = array();
            $data['corporate_user'] = $corporate_user_array;
            $data['roles'] = $roles;

            return response()->json(['data' => $data], 200);
        }
        return response()->json(['error' => 'invalid_role'], 401);
    }

    public function updateUser(Request $request, $id)
    {

        $user = JWTAuth::parseToken()->authenticate();
        if ($user->hasRole('manager')) {
            $this->validate($request, [
                'email' => 'required|email|unique:corporate_users',
                'name' => 'required|string|max:50',
                'password' => 'required',
                'job_title' => 'required',
                'mobile' => 'required',
                'status' => 'required',
            ]);

            $corporate_user = CorporateUser::find($id);
            $corporate_user->name = $request->name;
            $corporate_user->job_title = $request->job_title;
            $corporate_user->mobile = $request->mobile;
            $corporate_user->email = $request->email;
            $corporate_user->status = $request->status;
            $corporate_user->modified_at = time();
            $corporate_user->modified_by = $user->id;
            $roles = explode(",", $request->roles);
            $corporate_user->syncRoles($roles);
            $corporate_user->save();

            return response()->json(['data' => 'success'], 200);

        }
        return response()->json(['error' => 'invalid_role'], 401);
    }

    public function addUser()
    {
        $user = JWTAuth::parseToken()->authenticate();
        if ($user->hasRole('manager')) {

            $roles = Role::where('guard_name', 'web')->get('name');

            return response()->json(['data' => $roles], 200);

        }
        return response()->json(['error' => 'invalid_role'], 401);
    }

    public function insertUser(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        if ($user->hasRole('manager')) {

            $this->validate($request, [
                'email' => 'required|email|unique:corporate_users',
                'name' => 'required|string|max:50',
                'password' => 'required',
                'job_title' => 'required',
                'mobile' => 'required',
                'status' => 'required',
            ]);

            $corporate_user = new CorporateUser();
            $corporate_user->name = $request->name;
            $corporate_user->job_title = $request->job_title;
            $corporate_user->mobile = $request->mobile;
            $corporate_user->email = $request->email;
            $corporate_user->password = $request->password;
            $corporate_user->password_expire_date = time();
            $corporate_user->active_role = 1;
            $corporate_user->corporate_id = $user->corporate_id;
            $corporate_user->status = $request->status;
            $roles = explode(",", $request->roles);
            $corporate_user->syncRoles($roles);

            $corporate_user->save();

            return response()->json(['data' => 'success'], 200);

        }
        return response()->json(['error' => 'invalid_role'], 401);
    }

    //test
    public function assign(Request $request)
    {
        $user = CorporateUser::whereId(12)->firstOrFail()->syncRoles(['employee', 'manager']);
    }

}
