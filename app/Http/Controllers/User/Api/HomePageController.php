<?php

namespace App\Http\Controllers\User\Api;

use App\Http\Controllers\Controller;
use App\Models\Corporate\CorporateStoreByDay;
use App\Models\Item\Category;
use App\Models\Restaurant\RestaurantStore;
use Carbon\Carbon;
use Illuminate\Http\Request;
use JWTAuth;
use App\Traits\SettingTrait;


class HomePageController extends Controller
{
    use SettingTrait;

    public function getHomeData(Request $request)
    {
        $language_id = $request->language_id;

        $user = JWTAuth::user();

        if ($user->hasRole('employee')) {
            $data = array();

            //get days
            Carbon::setWeekStartsAt(Carbon::SATURDAY);
            Carbon::setWeekEndsAt(Carbon::FRIDAY);

            for ($i = 0; $i < 7; $i++) {
                $day[] = Carbon::now()->startOfWeek()->addDays($i)->format('D d-m-Y');
            }
            $all_days = collect($day);

            $corporate_days = CorporateStoreByDay::where('corporate_id', $user->corporate_id)->groupBy('day')->pluck('day')->toArray();

            $all_days = $all_days->map(function ($day) use ($corporate_days) {
                $day_name = Carbon::parse($day)->format('D');
                if (in_array($day_name, $corporate_days)) {

                    if (Carbon::now()->format('Y-m-d') > Carbon::parse($day)->format('Y-m-d')) {
                        $disabled = 1;
                        $selected = 0;
                    }


                        else if (Carbon::now()->format('Y-m-d') == Carbon::parse($day)->format('Y-m-d') ) {

                        $disabled = 0;
                        $selected = 1;
                    }

                        else {
                        $disabled = 0;
                        $selected = 0;}
                    return [
                        'day' => $day,
                        'is_disabled' => $disabled,
                        'is_selected' => $selected,
                    ];
                } else {
                    return;
                }

            });

            $data['days'] = collect($all_days->filter()->values());

            //get restaurants for today
            $today = Carbon::now()->format('D');

            $restaurants_ids_for_today = CorporateStoreByDay::where('corporate_id', $user->corporate_id)->where('day', $today)->pluck('restaurant_store_id');

            $corporate_stores = RestaurantStore::with(["restaurant_store_translation" => function ($q) use ($language_id) {
                $q->where('language_id', '=', $language_id);
            }])->whereIn('id', $restaurants_ids_for_today)->get();

            $corporate_stores = $corporate_stores->map(function ($store) {
                return [
                    'id' => $store->id,
                    'logo' => $store->logo,
                  'name' => $store->restaurant_store_translation[0]->name,
                  'image' => $store->image,
                  'description' =>$store->restaurant_store_translation[0]->description,
                ];
            });
            $data['restaurants'] = $corporate_stores;
            //get corporate categories , category settings
            $corporate_categories = Category::whereHas('corporate_setting', function ($q) use ($user)  {
                $q->where('corporate_id', '=', $user->corporate_id);
            })->with(['corporate_setting' => function ($q)   use ($user) {
                $q->where('corporate_id', '=', $user->corporate_id);
            }])->with(["category_translation" => function ($q) use ($language_id) {
                $q->where('language_id', '=', $language_id);
            }])
                ->get();


            $corporate_categories = $corporate_categories->map(function ($category) {
                return [
                    'id' => $category->id,
                    'logo' => $category->image,

                    'name' => $category->category_translation[0]->name,
                    'corporate_setting' => $category->corporate_setting->map(function ($setting) {
                        return [
                            'order_time_from' => Carbon::parse($setting->order_time_from)->format('H:i a'),
                            'order_time_to' =>Carbon::parse($setting->order_time_to)->format('H:i a') ,
                            'delivery_time_from' => Carbon::parse($setting->delivery_time_from)->format('H:i a') ,
                            'delivery_time_to' => Carbon::parse($setting->delivery_time_to)->format('H:i a') ,
                        ];
                    }),
                ];
            });
            $data['corporate_categories'] = $corporate_categories;
            return response()->json(['data' => $data], 200);
        }

        return response()->json(['error' => 'invalid_role'], 401);
    }
}
