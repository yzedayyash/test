<?php

namespace App\Http\Controllers\User\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Item\Item;
use JWTAuth;
use App\Traits\SettingTrait;
class ItemController extends Controller
{
    use SettingTrait;

    public function getItems(Request $request){


        $user = JWTAuth::user();

        if ($user->hasRole('employee')) {
            $data = array();
            $this->validate($request, [
                'category_id' => 'required',
                'day' => 'required',
                'restaurant_store_id' => 'required',
                'language_id' => 'required',

            ]);
            $language_id = $request->language_id;
            $category_id = $request->category_id;
            $day = $request->day;
            $restaurant_store_id = $request->restaurant_store_id;
            $price_unit = $this->getSetting('price_unit', $language_id);


            $items = Item::where('category_id' ,$category_id )
            ->where('restaurant_store_id' , $restaurant_store_id)
             ->whereHas('item_corporate', function ($q) use ($user , $day ) {
                $q->where('corporate_id', '=', $user->corporate_id)
                ->where('day' , $day );
            })->with(["item_translation" => function ($q) use ($language_id) {
                $q->where('language_id', '=', $language_id);
            }])->with(["item_unit.item_unit_translation" => function ($q) use ($language_id) {
                $q->where('language_id', '=', $language_id);
            }])
            ->with(["item_specification.item_specification_translation" => function ($q) use ($language_id) {
                $q->where('language_id', '=', $language_id);
            }])
            ->with('item_image')
            ->get();


            $items = $items->map(function ($item) use ($price_unit) {
                return [
                    'id' => $item->id,
                    'old_price' => $item->old_price,
                    'price' => $item->price,
                    'quantity' => $item->quantity_per_day,
                    'name' => $item->item_translation[0]->name,
                    'description' => $item->item_translation[0]->description,
                    'thumb' =>  $item->thumb,
                    'unit' => $item->item_unit->item_unit_translation[0]->name,
                    'price_unit'=>$price_unit,
                    'specifications' => $item->item_specification->map(function ($spec){
                       return [
                           'name' => $spec->item_specification_translation[0]->name,
                           'color' => '555555'

                       ];
                    }),
                    'item_images' => $item->item_image->map(function ($image){
                        return [
                            'image' => $image->image
                        ];
                     }),
                ];
            });
            return response()->json(['data' => $items], 200);
        }

        return response()->json(['error' => 'invalid_role'], 401);
    }
    }

