<?php

namespace App\Http\Controllers\User\Api;

use App\Http\Controllers\Controller;
use App\Models\Corporate\CorporateOrder;
use App\Models\Item\Item;
use App\Models\Corporate\CorporateOrderItem;
use App\Models\Corporate\CorporateCategorySetting;


use Illuminate\Http\Request;
use JWTAuth;
use App\Models\Corporate\CorporateBasket;
use App\Models\Corporate\OrderPayment;

use Omnipay\Omnipay;
use App\Traits\SettingTrait;

class OrderController extends Controller
{
    use SettingTrait;

    public function index(Request $request )
    {
        $user = JWTAuth::user();
        $language_id = $request->language_id;
        $price_unit = $this->getSetting('price_unit', $language_id);

        if ($user->hasRole('employee')) {
            $data = CorporateOrder::with('corporate_order_item')
                ->with('corporate_order_item.item')
                ->with(["corporate_order_item.item.item_translation" => function ($q) use ($language_id) {
                    $q->where('language_id', '=', $language_id);
                }])
                ->with(["corporate_order_item.item.item_unit.item_unit_translation" => function ($q) use ($language_id) {
                    $q->where('language_id', '=', $language_id);
                }])
                ->with('order_status')

                ->with(["order_status.corporate_order_translation" => function ($q) use ($language_id) {
                    $q->where('language_id', '=', $language_id);
                }])

                ->where('corporate_user_id', $user->id)
                ->get();
            $data = $data->map(function ($order) use ($price_unit) {


                return [
                    'id' => $order->id,
                    'note' => $order->note,
                    'price' => $order->price,
                    'price_unit' => $price_unit,
                    'status' => $order->order_status->corporate_order_translation[0]->name,
                    'corporate_order_item' => $order->corporate_order_item->map(function ($basket) use ($price_unit) {

                        return [
                            'quantity' => $basket->quantity,
                            'item' => collect([
                                'id' => $basket->item->id,
                                'old_price' => $basket->item->old_price,
                                'price' => $basket->item->price,
                                'name' => $basket->item->item_translation[0]->name,
                                'description' => $basket->item->item_translation[0]->description,
                                'unit' => $basket->item->item_unit->item_unit_translation[0]->name,
                                'price_unit' => $price_unit,
                                'item_total' => $basket->item->price * $basket->quantity,
                            ])];
                    }),

                ];

            });

            return response()->json(['data' => $data], 200);
        }
        return response()->json(['error' => 'invalid_role'], 401);

    }

    public function store(Request $request)
    {
        $user = JWTAuth::user();

        if ($user->hasRole('employee')) {
            $user_basket = CorporateBasket::with('items')
            ->where('corporate_user_id', $user->id)->get();
            if(!count($user_basket ) > 0){
              return response()->json(['error' => 'cart is empty'], 400);

            }
            $total = 0;
            $category_id =  $user_basket[0]->category_id;
            $store_id =  $user_basket[0]->restaurant_store_id;
            $date =  $user_basket[0]->date;

            $basket = $user_basket->map(function ($basket) use (&$total) {
                $total += $basket->items->price * $basket->quantity;
                return [
                    'item_id' => $basket->item_id,
                    'quantity' => $basket->quantity,
                ];
            });
            $time_details = CorporateCategorySetting::where('corporate_id' , $user->corporate_id)->where('category_id' , $category_id)->first();
            if(!$time_details ){
            return response()->json(['error' => 'someting went wrong'], 400);

            }
            $order = new CorporateOrder();
            $order->price  = $total ;
            $order->corporate_user_id  = $user->id;
            $order->corporate_order_status_id  = 1 ;
            $order->note  = $request->note ;
            $order->restaurant_store_id  = $store_id ;
            $order->category_id  = $category_id ;
            $order->order_date   = $date ;
            $order->corporate_id  = $user->corporate_id;
            $order->delivery_time_from  = $time_details->delivery_time_from;
            $order->delivery_time_to  = $time_details->delivery_time_to;

            //need to add date
            $order->save();



            $basket2 = $user_basket->map(function ($basket) use ($order) {
                return [
                    'item_id' => $basket->item_id,
                    'quantity' => $basket->quantity,
                    'corporate_order_id' => $order->id
                ];
            });
            CorporateOrderItem::insert($basket2->toArray());

            $payment_data = [
                'total' => $total,
                'id' => $order->id,
                'os' => $request->os,
            ];
            CorporateBasket::where('corporate_user_id', $user->id)->delete();
            return  $this->pay($payment_data);
        }
        return response()->json(['error' => 'invalid_role'], 401);

    }

    public function teststore(Request $request)
    {
        // $this->pay();
        // $user = JWTAuth::user();

        // if ($user->hasRole('employee')) {
            $user_basket = CorporateBasket::with('items')
            ->where('corporate_user_id', 2)->get();
            $total = 0;

            $basket = $user_basket->map(function ($basket) use (&$total) {
                $total += $basket->items->price * $basket->quantity;
                return [
                    'item_id' => $basket->item_id,
                    'quantity' => $basket->quantity,

                ];
            });
            $order = new CorporateOrder();
            $order->price  = $total ;
            $order->corporate_user_id  = 2;
            $order->corporate_order_status_id  = 1 ;
            $order->note  = $request->note ;
            $order->save();



            $basket2 = $user_basket->map(function ($basket) use ($order) {
                return [
                    'item_id' => $basket->item_id,
                    'quantity' => $basket->quantity,
                    'corporate_order_id' => $order->id
                ];
            });
            CorporateOrderItem::insert($basket2->toArray());

            $payment_data = [
                'total' => $total,
                'os' => 'ss',
                'id' => $order->id,
            ];
            $this->pay($payment_data);

        // }
        // return response()->json(['error' => 'invalid_role'], 401);

    }
    public function pay($data){

        $gateway = Omnipay::create('Migs_ThreeParty');
        $gateway->setMerchantId('');
        $gateway->setMerchantAccessCode('');
        $gateway->setSecureHash('');



        $response = $gateway->purchase(array(
            'amount' => (double)$data['total'], // amount should be greater than zero
            'currency' => 'QAR',
            'transactionId' => (int)$data['id'], // replace this for your reference # such as invoice reference #
            'returnURL' => \URL::to('/api/complete_payment')
        ))->send();
     if ($response->isRedirect()) {
        $url = $response->getRedirectUrl();

         if($data['os'] == 'mobile') {
            return response()->json(['data' => $url], 200);
         }else {
         return $response->redirect();
         }
        }


    }

    public function completePayment(Request $request){
        $gateway = Omnipay::create('Migs_ThreeParty');
        $gateway->setMerchantId('');
        $gateway->setMerchantAccessCode('');
        $gateway->setSecureHash('');

        $response = $gateway->completePurchase()->send();

        $order = $response->getData();

        $transaction = $order['vpc_TransactionNo'];
        $responce = $order['vpc_TxnResponseCode'];
        $amount = $order['vpc_Amount'];
        $card_type = $order['vpc_Card'];
        $order_id = $order['vpc_MerchTxnRef'];
        $receipt_no = $order['vpc_ReceiptNo'];
        $channel_ref_no = $order['vpc_BatchNo'];

        $order_paymnet = new OrderPayment();
        $order_paymnet->order_id = $order_id;
        $order_paymnet->status = $responce;
        $order_paymnet->receipt_code = $receipt_no;
        $order_paymnet->receipt_date = time();
        $order_paymnet->payment_channel = $card_type;
        $order_paymnet->channel_ref_no = $channel_ref_no;
        $order_paymnet->paid_amount = $amount/100;
        $order_paymnet->save();
        if ($response->isSuccessful()) {
           CorporateOrder::where('id' , $order_id)->update(['status' , 2]);
        return 1;
        }else{
        return  0;
    } }
}
