<?php

namespace App\Http\Controllers\User\Api;

use App\Http\Controllers\Controller;
use App\Models\Corporate\Corporate;
use App\Models\Corporate\CorporateUser;
use App\Models\Corporate\InviteCorporateUser;
use App\Models\Corporate\PasswordReset;
use App\Notifications\PasswordResetRequest;
use App\Traits\NotificationTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
    use NotificationTrait;
    public function authenticate(Request $request)
    {

        $credentials = $request->only('email', 'password');

        try {
            if (!$token = JWTAuth::claims(['ut' => 'user'])->attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 404);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        $user = JWTAuth::user();
        // JWTAuth::setToken($token)->invalidate();
        $otp = mt_rand(100000, 999999);

        if ($user->is_validated == 0) {
            $details = [

                'greeting' => 'Hi ' . $user->email,

                'body' => 'Your OTP ' . $otp,

                'thanks' => 'Thank you for using test',

                'actionText' => '',

                // 'actionURL' =>  '',
                'otp' => $otp,
                'phone' => $user->mobile,

            ];

            CorporateUser::where('mobile' , $user->mobile)->where('email' , $user->email)->update([
                'otp' => Hash::make($otp)
            ]);
            $this->OTPNotification($user, $details);
            JWTAuth::setToken($token)->refresh();

            return response()->json(['token' => null, 'verified' => 0, 'user' => $user], 200);

        }
        return response()->json(['token' => $token, 'verified' => 1], 200);
    }

    public function register(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|email|unique:corporate_users',
            'name' => 'required|string|max:50',
            'password' => 'required',
            'job_title' => 'required',
            'mobile' => 'required',

        ]);
        $otp = mt_rand(100000, 999999);
        $code = $request->corporate_code;
        $corporate = Corporate::where('code', $code)->first();
        if ($corporate) {
            $user = new CorporateUser();
            $user->name = $request->name;
            $user->job_title = $request->job_title;
            $user->mobile = $request->mobile;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->password_expire_date = time();
            $user->active_role = 1;
            $user->corporate_id = $corporate->id;
            $user->status = 1;
            $user->otp = Hash::make($otp);

            $user->syncRoles(['employee']);

            $user->save();

            $details = [

                'greeting' => 'Hi ' . $user->email,

                'body' => 'Your OTP ' . $otp,

                'thanks' => 'Thank you for using test',

                'actionText' => '',

                // 'actionURL' =>  '',
                'otp' => $otp,
                'phone' => $user->mobile,

            ];
            $this->OTPNotification($user, $details);

            $user = CorporateUser::where('email', $request->email)->first();

            return response()->json(compact('user' ), 201);

        } else {
            return response()->json([
                'message' => 'corporate_not_found',
            ],404) ;
           }
    }

    public function updateProfile(Request $request)
    {

        $user = JWTAuth::user();

        if ($user->hasRole('employee')) {
            $this->validate($request, [
                'name' => 'required|string|max:50',
                'job_title' => 'required',
                'mobile' => 'required',
            ]);

            $corporate_user = CorporateUser::find($user->id);
            $corporate_user->name = $request->name;
            $corporate_user->job_title = $request->job_title;
            $corporate_user->mobile = $request->mobile;
            $corporate_user->modified_at = time();
            $corporate_user->modified_by = $user->id;
            $corporate_user->save();

            return response()->json(['user' => $corporate_user], 200);

        }
        return response()->json(['error' => 'invalid_role'], 401);
    }

    public function forgetPassword(Request $request)
    {

        $request->validate([
            'email' => 'required|string|email',
        ]);
        $token = Str::random(60);
        $otp = mt_rand(100000, 999999);
        $user = CorporateUser::where('email', $request->email)->first();
        if (!$user) {
            return response()->json([
                'message' => 'We cant find a user with that e-mail address.',
            ], 404);
        }

        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $user->email],
            ['email' => $user->email,
                'token' => $token,
                'otp' => Hash::make($otp),

                'created_at' => Carbon::now(),
            ]
        );
        if ($user && $passwordReset) {
            $user->notify(
                new PasswordResetRequest($token, $otp));
        }

        return response()->json([
            'message' => 'We have e-mailed your password reset link!',
        ]);
    }

    public function resetPassword(Request $request, $token)
    {

        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string|confirmed',
        ]);
        $passwordReset = PasswordReset::where([
            ['token', $request->token],
            ['email', $request->email],
        ])->first();
        if (!$passwordReset) {
            return response()->json([
                'message' => 'This password reset token is invalid.',
            ], 404);
        }

        $user = CorporateUser::where('email', $passwordReset->email)->first();
        if (!$user) {
            return response()->json([
                'message' => 'We cant find a user with that e-mail address.',
            ], 404);
        }

        $user->password = bcrypt($request->password);
        $user->save();
        $passwordReset->delete();
        $token = JWTAuth::claims(['ut' => 'user'])->fromUser($user);
        //$token = JWTAuth::getToken();

        return response()->json(compact('user', 'token'), 201);
    }

    public function resetPasswordFromApplication(Request $request)
    {

        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string|confirmed',
            'otp' => 'required',
        ]);
        $passwordReset = PasswordReset::where([
            ['email', $request->email],
        ])->first();
        if (!$passwordReset) {
            return response()->json([
                'message' => 'This password reset otp is invalid.',
            ], 404);
        }

        $user = CorporateUser::where('email', $passwordReset->email)->first();
        if (!$user) {
            return response()->json([
                'message' => 'We cant find a user with that e-mail address.',
            ], 404);
        }

        if (!Hash::check($request->otp, $passwordReset->otp)) {


            return response()->json([
                'message' => 'This password reset otp is invalid.',
            ], 404);
        }

        $user->password = bcrypt($request->password);

        $user->save();
        $passwordReset->delete();
        $token = JWTAuth::claims(['ut' => 'user'])->fromUser($user);
        //$token = JWTAuth::getToken();

        return response()->json(compact('user', 'token'), 201);
    }

    public function updatePassword(Request $request)
    {

        $user = JWTAuth::user();
        if ($user->hasRole('employee')) {

            $request->validate([
                'password' => 'required|string|confirmed',
            ]);

            $corporate_user = CorporateUser::find($user->id);

            $corporate_user->password = bcrypt($request->password);

            $corporate_user->save();
            return response()->json(['data' => 'success'], 200);
        }
        return response()->json(['error' => 'invalid_role'], 401);
    }

    public function getAuthenticatedUser()
    {

        try {

            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json([
                    'message' => 'User Not Found.',
                ], 404);            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        return response()->json(compact('user'));
    }

    public function invite_employee(Request $request, $token)
    {
        $data = InviteCorporateUser::where('token', $token)->first();
        return response()->json(['data' => $data], 200);

    }

    public function verify_user(Request $request)
    {
        $request->validate([
            'otp' => 'required|string',
            'mobile' => 'required',
            'email' => 'required',
        ]);

        $user = CorporateUser::where('mobile', $request->mobile)->where('email', $request->email)->first();
        $otp = $user->otp;
        $requestd_otp = $request->otp;

        if (Hash::check($requestd_otp, $otp)) {

            $user->is_validated = 1;
            $user->save();

            $token = JWTAuth::claims(['ut' => 'user'])->fromUser($user);
        //    return $token;
       //     $token = JWTAuth::getToken();


            return response()->json(['data' => 'success', 'token' => $token], 200);

        } else {
            return response()->json(['error' => 'invalid otp'], 401);

        }

    }


    public function register_device(Request $request){

        if($request->header('Authorization')) { //he is a user :)

            $user = JWTAuth::toUser(JWTAuth::getToken());
            return $user;
        }

        else  {  //he isNOT a user :)
        return 1;}
        return response()->json(['data' => 'success'], 200);

    }

}
