<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Corporate\CorporateUser;
use App\Traits\NotificationTrait;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\URL;
use Omnipay\Omnipay;

class UserController extends Controller
{
    use NotificationTrait;
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function index()
    {
        return view('user.home');
    }

    public function sendNotification2()
    {

        $users = CorporateUser::all();

        $details = [

            'greeting' => 'Hi Yazeed',

            'body' => 'This is my first notification from test',

            'thanks' => 'Thank you for using test',

            'actionText' => 'View My Site',

            'actionURL' => url('/'),

            'order_id' => 101,

        ];

        $this->SMSNotification($users,$details);


    }

    public function sendNotification()
    {


        $users = CorporateUser::all();

        $details = [

            'title' => 'Hi Yazeed',

            'text' => 'This is my first notification from test',

            'player_id' => ['e6994635-a0bc-4e0f-a108-cd2af0056ef6']

        ];


        $this->OneSignalNotification($details);


    }

    public function TestGuzzle(){

        $url = "https://jsonplaceholder.typicode.com/todos/1";

        $client = new Client();

        $request = $client->get($url);

        $response = $request->getBody();

        dd($request->getStatusCode());


        dd(json_decode($response));


}

public function pay(){

    $gateway = Omnipay::create('Migs_ThreeParty');
    $gateway->setMerchantId('');
    $gateway->setMerchantAccessCode('');
    $gateway->setSecureHash('');

    $response = $gateway->purchase(array(
        'amount' => 10, // amount should be greater than zero
        'currency' => 'QAR',
        'transactionId' => 12151, // replace this for your reference # such as invoice reference #
        'returnURL' => URL::to('/home')
    ))->send();
    if ($response->isSuccessful()) {
        // payment is complete
    } elseif ($response->isRedirect()) {
        $response->redirect(); // this will automatically forward the customer
    } else {
        // not successful
    }


}
}
