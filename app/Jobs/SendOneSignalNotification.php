<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendOneSignalNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $text = $this->data['text'];
        $title = $this->data['title'];

        $player_id = $this->data['player_id'];

        $data = array('from'=>'employee');

        $content = array("en" => $text);
        $title = array("en" => $title);
        $subTitle = array("en" => $text);


        $fields = array(
            'app_id' => env("ONE_SIGNAL_APP_ID", ""),
            'include_player_ids' => $player_id,
            'data' => $data,
            'contents' => $content,
            'headings' => $title,
        );
        $fields = json_encode($fields);
        // print("\nJSON sent:\n");
        // print($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8', 'Authorization: Basic ' . env("ONE_SIGNAL_REST_KEY", "")));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($ch);
        curl_close($ch);
        // $return["allresponses"] = $response;
        // $return = json_encode($return);


    }
}
