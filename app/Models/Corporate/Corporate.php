<?php

namespace App\Models\Corporate;

use Illuminate\Database\Eloquent\Model;

class Corporate extends Model
{
    public $timestamps = false;
    public function corporate_user()
    {
        return $this->hasMany('App\Models\Corporate\CorporateUser');
    }
    public function corporate_department()
    {
        return $this->hasMany('App\Models\Corporate\CorporateDepartment');
    }
    public function corporate_store_by_day()
    {
        return $this->belongsToMany('App\Models\Restaurant\RestaurantStore' , 'corporate_store_by_days')->withPivot('day');
    }


    public function corporate_items()
    {
        return $this->belongsToMany('App\Models\Item\Item', 'corporate_item')->withPivot('day');
    }

    public function corporate_setting()
    {
        return $this->hasMany('App\Models\Corporate\CorporateCategorySetting');
    }
}
