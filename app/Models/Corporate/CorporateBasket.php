<?php

namespace App\Models\Corporate;

use Illuminate\Database\Eloquent\Model;

class CorporateBasket extends Model
{
    public $timestamps = false;

    public function items()
    {
        return $this->belongsTo('App\Models\Item\Item' , 'item_id');
    }

    public function corporate_user()
    {
        return $this->belongsTo('App\Models\Corporate\CorporateUser');
    }
}
