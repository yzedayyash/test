<?php

namespace App\Models\Corporate;

use Illuminate\Database\Eloquent\Model;

class CorporateCategorySetting extends Model
{
    public $timestamps = false;

    public function category()
    {
        return $this->belongsTo('App\Models\Item\Category');
    }


    public function corporate()
    {
        return $this->belongsTo('App\Models\Corporate\Corporate');
    }
}
