<?php

namespace App\Models\Corporate;

use Illuminate\Database\Eloquent\Model;

class CorporateDepartment extends Model
{
    public $timestamps = false;

    public function corporate()
    {
        return $this->belongsTo('App\Models\Corporate\Corporate');
    }

    public function department_user()
    {
        return $this->belongsToMany('App\Models\Corporate\CorporateUser', 'corporate_department_corporate_user');
    }

    public function sub_department(){
        return $this->hasMany('App\Models\Corporate\CorporateDepartment' , 'parent_id');
    }

    public function sub_departments(){
        return $this->sub_department()->with('sub_departments')->withCount('department_user');
    }


    public function parent_department(){
        return $this->belongsTo('App\Models\Corporate\CorporateDepartment' , 'parent_id');
    }

    public function corporate_department_translation()
    {
        return $this->hasMany('App\Models\Corporate\CorporateDepartmentTranslation');
    }
}
