<?php

namespace App\Models\Corporate;

use Illuminate\Database\Eloquent\Model;

class CorporateDepartmentTranslation extends Model
{
    public $timestamps = false;


    public function corporate_department()
    {
        return $this->belongsTo('App\Models\Corporate\CorporateDepartment');
    }
}
