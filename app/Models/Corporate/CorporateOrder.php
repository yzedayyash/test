<?php

namespace App\Models\Corporate;

use Illuminate\Database\Eloquent\Model;

class CorporateOrder extends Model
{
    public $timestamps = false;

    public function corporate_user()
    {
        return $this->belongsTo('App\Models\Corporate\CorporateUser' , 'corporate_user_id');
    }

    public function delivery_boy_data()
    {
        return $this->belongsTo('App\Models\Restaurant\RestaurantUser' , 'delivery_boy');
    }

    public function order_status()
    {
        return $this->belongsTo('App\Models\Corporate\CorporateOrderStatus' , 'corporate_order_status_id');
    }
    public function corporate_order_item()
    {
        return $this->hasMany('App\Models\Corporate\CorporateOrderItem');
    }

    public function corporate_order_status_history()
    {
        return $this->belongsToMany('App\Models\Corporate\CorporateOrderStatus' , 'order_status_histories')->withPivot('created_at');
    }
}
