<?php

namespace App\Models\Corporate;

use Illuminate\Database\Eloquent\Model;

class CorporateOrderItem extends Model
{
    public $timestamps = false;
    public function item()
    {
        return $this->belongsTo('App\Models\Item\Item');
    }

    public function corporate_order()
    {
        return $this->belongsTo('App\Models\Corporate\CorporateOrder');
    }


}
