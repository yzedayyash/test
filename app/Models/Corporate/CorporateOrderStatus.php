<?php

namespace App\Models\Corporate;

use Illuminate\Database\Eloquent\Model;

class CorporateOrderStatus extends Model
{
    public function corporate_order()
    {
        return $this->hasMany('App\Models\Corporate\CorporateOrder');
    }

    public function corporate_order_translation()
    {
        return $this->hasMany('App\Models\Corporate\CorporateOrderStatusTranslate');
    }

    public function corporate_order_status_history()
    {
        return $this->belongsToMany('App\Models\Corporate\CorporateOrder' , 'order_status_histories');
    }
}

