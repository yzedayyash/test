<?php

namespace App\Models\Corporate;

use Illuminate\Database\Eloquent\Model;

class CorporateOrderStatusTranslate extends Model
{
    public function order_status()
    {
        return $this->belongsTo('App\Models\Corporate\CorporateOrderStatus');
    }
}
