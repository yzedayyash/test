<?php

namespace App\Models\Corporate;

use Illuminate\Database\Eloquent\Model;

class CorporateStoreByDay extends Model
{
    public $timestamps = false;

    public function corporate()
    {
        return $this->belongsTo('App\Models\Corporate\Corporate');
    }
}
