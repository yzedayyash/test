<?php

namespace App\Models\Corporate;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;
class CorporateUser extends Authenticatable implements JWTSubject
{
    use Notifiable, HasRoles;
    protected $table = 'corporate_users';
    protected $guard = 'web';
    public $timestamps = false;

    public function corporate()
    {
        return $this->belongsTo('App\Models\Corporate\Corporate');
    }

    public function corporate_department()
    {
        return $this->belongsToMany('App\Models\Corporate\CorporateDepartment', 'corporate_department_corporate_user');
    }

    public function corporate_basket()
    {
        return $this->hasMany('App\Models\Corporate\CorporateBasket');
    }

    public function restaurant_store_rate()
    {
        return $this->hasMany('App\Models\Restaurant\RestaurantStoreRate');
    }
    public function corporate_order()
    {
        return $this->hasMany('App\Models\Corporate\CorporateOrder');
    }
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
    protected $fillable = [
        'name',
        'email',
        'password',
    ];
}
