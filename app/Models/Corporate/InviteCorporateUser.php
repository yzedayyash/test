<?php

namespace App\Models\Corporate;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class InviteCorporateUser extends Model
{
    use Notifiable;

    protected $hidden = [
        'token',
        'created_at',
        'updated_at'
    ];
}
