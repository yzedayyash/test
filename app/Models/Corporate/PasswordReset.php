<?php

namespace App\Models\Corporate;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{
    protected $fillable = [
        'email', 'token' , 'otp'
    ];

    const UPDATED_AT = null;
}
