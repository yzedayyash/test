<?php

namespace App\Models\Item;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps = false;

    public function category_translation()
    {
        return $this->hasMany('App\Models\Item\CategoryTranslation');
    }

    public function item()
    {
        return $this->hasMany('App\Models\Item\Item');
    }

    public function corporate_setting()
    {
        return $this->hasMany('App\Models\Corporate\CorporateCategorySetting');
    }
}
