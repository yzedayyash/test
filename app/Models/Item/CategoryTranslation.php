<?php

namespace App\Models\Item;

use Illuminate\Database\Eloquent\Model;

class CategoryTranslation extends Model
{
    public $timestamps = false;

    public function category()
    {
        return $this->belongsTo('App\Models\Item\Category');
    }
}
