<?php

namespace App\Models\Item;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public $timestamps = false;


    public function item_unit()
    {
        return $this->belongsTo('App\Models\Item\ItemUnit');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Item\Category');
    }

    public function restaurant_store()
    {
        return $this->belongsTo('App\Models\Restaurant\RestaurantStore');
    }


    public function item_translation()
    {
        return $this->hasMany('App\Models\Item\ItemTranslation');
    }

    public function item_image()
    {
        return $this->hasMany('App\Models\Item\ItemImage');
    }
    public function item_specification()
    {
        return $this->belongsToMany('App\Models\Item\ItemSpecification' , 'item_item_specification');
    }
    public function item_corporate()
    {
        return $this->belongsToMany('App\Models\Corporate\Corporate', 'corporate_item');
    }

    public function corporate_basket()
    {
        return $this->hasMany('App\Models\Corporate\CorporateBasket');
    }

    public function corporate_order_item()
    {
        return $this->hasMany('App\Models\Corporate\CorporateOrderItem');
    }
}
