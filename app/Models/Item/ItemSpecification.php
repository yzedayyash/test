<?php

namespace App\Models\Item;

use Illuminate\Database\Eloquent\Model;

class ItemSpecification extends Model
{
    public function items()
    {
        return $this->belongsToMany('App\Models\Item\Item' , 'item_item_specification');
    }


    public function item_specification_translation()
    {
        return $this->hasMany('App\Models\Item\ItemSpecificationTranslation');
    }
}
