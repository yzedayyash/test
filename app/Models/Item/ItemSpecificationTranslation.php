<?php

namespace App\Models\Item;

use Illuminate\Database\Eloquent\Model;

class ItemSpecificationTranslation extends Model
{
    public function item_specification()
    {
        return $this->belongsTo('App\Models\Item\Item');
    }
}
