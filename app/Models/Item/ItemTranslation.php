<?php

namespace App\Models\Item;

use Illuminate\Database\Eloquent\Model;

class ItemTranslation extends Model
{
    public $timestamps = false;


    public function items()
    {
        return $this->belongsTo('App\Models\Item\Item');
    }

}
