<?php

namespace App\Models\Item;

use Illuminate\Database\Eloquent\Model;

class ItemUnit extends Model
{
    public $timestamps = false;

    public function item_unit_translation()
    {
        return $this->hasMany('App\Models\Item\ItemUnitTranslation');
    }

    public function item()
    {
        return $this->hasMany('App\Models\Item\Item');
    }
}
