<?php

namespace App\Models\Item;

use Illuminate\Database\Eloquent\Model;

class ItemUnitTranslation extends Model
{
    public $timestamps = false;

    public function item_unit()
    {
        return $this->belongsTo('App\Models\Item\ItemUnit');
    }
}
