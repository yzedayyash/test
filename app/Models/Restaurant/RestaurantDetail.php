<?php

namespace App\Models\Restaurant;

use Illuminate\Database\Eloquent\Model;

class RestaurantDetail extends Model
{
    public $timestamps = false;

    public function restaurant_stores()
    {
        return $this->hasMany('App\Models\Restaurant\RestaurantStore');
    }
}
