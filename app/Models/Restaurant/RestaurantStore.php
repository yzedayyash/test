<?php

namespace App\Models\Restaurant;

use Illuminate\Database\Eloquent\Model;

class RestaurantStore extends Model
{
    public $timestamps = false;

    public function restaurant_details()
    {
        return $this->belongsTo('App\Models\Restaurant\RestaurantDetail');
    }

    public function restaurant_store_translation()
    {
        return $this->hasMany('App\Models\Restaurant\RestaurantStoreTranslation');
    }

    public function item()
    {
        return $this->hasMany('App\Models\Item\Item');
    }

    public function restaurant_user()
    {
        return $this->hasMany('App\Models\Restaurant\RestaurantUser');
    }


    public function corporate_store_by_day()
    {
        return $this->belongsToMany('App\Models\Corporate\Corporate' , 'corporate_store_by_days');
    }

    public function restaurant_store_rate()
    {
        return $this->hasMany('App\Models\Restaurant\RestaurantStoreRate');
    }


}
