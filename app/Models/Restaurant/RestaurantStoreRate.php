<?php

namespace App\Models\Restaurant;

use Illuminate\Database\Eloquent\Model;

class RestaurantStoreRate extends Model
{
    public $timestamps = false;


    public function corporate_user()
    {
        return $this->belongsTo('App\Models\Corporate\CorporateUser');
    }

    public function restaurant_store()
    {
        return $this->belongsTo('App\Models\Restaurant\RestaurantStore');
    }
}
