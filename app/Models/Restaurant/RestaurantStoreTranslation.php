<?php

namespace App\Models\Restaurant;

use Illuminate\Database\Eloquent\Model;

class RestaurantStoreTranslation extends Model
{
        public $timestamps = false;

        public function restaurant_stores()
        {
            return $this->belongsTo('App\Models\Restaurant\RestaurantStore');
        }
}
