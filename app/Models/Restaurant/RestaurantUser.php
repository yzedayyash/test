<?php

namespace App\Models\Restaurant;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

use Tymon\JWTAuth\Contracts\JWTSubject;
class RestaurantUser extends Authenticatable implements JWTSubject
{

    use Notifiable, HasRoles;


    public function restaurant_store()
    {
        return $this->belongsTo('App\Models\Restaurant\RestaurantStore');
    }




    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }


    protected $table = 'restaurant_users';
    protected $guard_name = 'restaurant';
    public $timestamps = false;
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
