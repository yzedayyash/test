<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use App\Channels\SMSChannel;
use App\Channels\Messages\VoiceMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
class SMS extends Notification implements  ShouldQueue
{
    use Queueable;

    public $details;

    public function __construct($details)

    {

        $this->details = $details;

    }
    public function via($notifiable)
    {

        return [SMSChannel::class];
    }

    public function toSMS($notifiable)
    {
        return [
            $this->details
        ];
    }




}
