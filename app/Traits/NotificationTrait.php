<?php
namespace App\Traits;
use App\Notifications\OneSignal;
use App\Notifications\SMS;
use App\Notifications\Email;
use Carbon\Carbon;
use App\Jobs\SendOneSignalNotification;
trait NotificationTrait
{
    /**
     * @return string
     */
    public function OTPNotification($user,$details)
    {

            $user->notify(new SMS($details));
            $user->notify(new Email($details));



    }


    public function OneSignalNotification($data)
    {


        $notification = new SendOneSignalNotification($data);
        dispatch($notification);



    }

    public function OrderNotification($users,$details)
    {
        $when = Carbon::now()->addSecond();
        foreach ($users as $user) {
            $user->notify((new Email($details))->delay($when));
            $user->notify((new OneSignal($details))->delay($when));

        }


    }

    public function InvitiationEmail($details,$user)
    {

            $user->notify((new Email($details)));




    }


}
