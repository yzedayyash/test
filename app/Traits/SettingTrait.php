<?php
namespace App\Traits;

trait SettingTrait
{
    /**
     * @return string
     */
    public function getSetting($key,$language_id){

    return    \App\Models\Common\Setting::where('key', $key)->where('language_id', $language_id)->first()->value;
    }




}
