composer install
php artisan migrate
php artisan db:seed
//get .env file 
php artisan key:generate
php artisan jwt:secret
php artisan queue:work
php artisan config:cache
