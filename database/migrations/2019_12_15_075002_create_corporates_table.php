<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCorporatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('corporates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('primary_email');
            $table->string('secondary_email')->nullable()->default(null);
            $table->string('zip_code')->nullable()->default(null);
            $table->string('pobox')->nullable()->default(null);
            $table->string('website')->nullable()->default(null);
            $table->string('logofid')->nullable()->default(null);
            $table->string('phone');
            $table->string('company_name');
            $table->string('tender_number_type');
            $table->string('registration_type');
            $table->string('description');
            $table->string('address');
            $table->integer('created_at')->default(time());
            $table->integer('created_by')->nullable()->default(null);
            $table->integer('modified_at')->nullable()->default(null);
            $table->integer('modified_by')->nullable()->default(null);
            $table->integer('deleted')->nullable()->default(null);
            $table->integer('deleted_by')->nullable()->default(null);
            $table->integer('deleted_at')->nullable()->default(null);
            $table->integer('status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('corporates');
    }
}
