<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCorporateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('corporate_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('job_title')->nullable()->default(null);
            $table->string('nationality')->nullable()->default(null);
            $table->string('email');
            $table->string('password');
            $table->string('salt')->nullable()->default(null);
            $table->string('password_expire_date');
            $table->string('token')->nullable()->default(null);
            $table->string('token_expire_date')->nullable()->default(null);
            $table->string('login_token')->nullable()->default(null);
            $table->string('login_token_expire_date')->nullable()->default(null);
            $table->string('mobile')->nullable()->default(null);
            $table->string('phone')->nullable()->default(null);
            $table->integer('created_at')->default(time());
            $table->integer('created_by')->nullable()->default(null);
            $table->integer('modified_at')->nullable()->default(null);
            $table->integer('modified_by')->nullable()->default(null);
            $table->integer('deleted')->nullable()->default(null);
            $table->integer('deleted_by')->nullable()->default(null);
            $table->integer('deleted_at')->nullable()->default(null);
            $table->integer('status')->default(0);
            $table->integer('active_role');
            $table->unsignedInteger('corporate_id');
            $table->rememberToken();


            $table->foreign('corporate_id')
                ->references('id')
                ->on('corporates');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    { Schema::table('corporate_users', function(Blueprint $table){
        $table->dropForeign('corporate_users_corporate_id_foreign');
        });

        Schema::dropIfExists('corporate_users');
    }
}
