<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestaurantDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('logo');
            $table->string('phone');
            $table->string('email');
            $table->integer('created_at')->default(time());
            $table->integer('created_by')->nullable()->default(null);
            $table->integer('modified_at')->nullable()->default(null);
            $table->integer('modified_by')->nullable()->default(null);
            $table->integer('deleted')->nullable()->default(null);
            $table->integer('deleted_by')->nullable()->default(null);
            $table->integer('deleted_at')->nullable()->default(null);
            $table->integer('status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_details');
    }
}
