<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestaurantStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_stores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('logo');
            $table->string('phone');
            $table->string('email');
            $table->string('adderss');
            $table->integer('created_at')->default(time());
            $table->integer('created_by')->nullable()->default(null);
            $table->integer('modified_at')->nullable()->default(null);
            $table->integer('modified_by')->nullable()->default(null);
            $table->integer('deleted')->nullable()->default(null);
            $table->integer('deleted_by')->nullable()->default(null);
            $table->integer('deleted_at')->nullable()->default(null);
            $table->integer('status')->default(0);

            $table->unsignedInteger('restaurant_detail_id');


            $table->foreign('restaurant_detail_id')
                ->references('id')
                ->on('restaurant_details');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('restaurant_stores', function(Blueprint $table){
            $table->dropForeign('restaurant_stores_restaurant_detail_id_foreign');
            });
        Schema::dropIfExists('restaurant_stores');
    }
}
