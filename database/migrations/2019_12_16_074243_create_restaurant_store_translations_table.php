<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestaurantStoreTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_store_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('language_id');
            $table->unsignedInteger('restaurant_store_id');

            $table->foreign('restaurant_store_id')
            ->references('id')
            ->on('restaurant_stores');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('restaurant_store_translations', function(Blueprint $table){
            $table->dropForeign('restaurant_store_translations_restaurant_store_id_foreign');
            });
        Schema::dropIfExists('restaurant_store_translations');
    }
}
