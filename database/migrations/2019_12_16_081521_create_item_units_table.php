<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_units', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->integer('created_at')->default(time());

            $table->integer('created_by')->nullable()->default(null);
            $table->integer('modified_at')->nullable()->default(null);
            $table->integer('modified_by')->nullable()->default(null);
            $table->integer('deleted')->nullable()->default(null);
            $table->integer('deleted_by')->nullable()->default(null);
            $table->integer('deleted_at')->nullable()->default(null);
            $table->integer('status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_units');
    }
}
