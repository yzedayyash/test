<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemUnitTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_unit_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('language_id');
            $table->unsignedInteger('item_unit_id');

            $table->foreign('item_unit_id')
            ->references('id')
            ->on('item_units');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_unit_translations', function(Blueprint $table){
            $table->dropForeign('item_unit_translations_item_unit_id_foreign');
            });
        Schema::dropIfExists('item_unit_translations');
    }
}
