<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->float('price', 8, 2);
            $table->float('old_price', 8, 2);
            $table->integer('quantity_per_day');

            $table->integer('created_at')->default(time());
            $table->integer('created_by')->nullable()->default(null);
            $table->integer('modified_at')->nullable()->default(null);
            $table->integer('modified_by')->nullable()->default(null);
            $table->integer('deleted')->nullable()->default(null);
            $table->integer('deleted_by')->nullable()->default(null);
            $table->integer('deleted_at')->nullable()->default(null);
            $table->integer('status')->default(0);

            $table->unsignedInteger('item_unit_id');

            $table->foreign('item_unit_id')
            ->references('id')
            ->on('item_units');

            $table->unsignedInteger('category_id');

            $table->foreign('category_id')
            ->references('id')
            ->on('categories');

            $table->unsignedInteger('restaurant_store_id');

            $table->foreign('restaurant_store_id')
            ->references('id')
            ->on('restaurant_stores');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items', function(Blueprint $table){
            $table->dropForeign('items_item_unit_id_foreign');
            $table->dropForeign('items_restaurant_store_id_foreign');
            $table->dropForeign('items_category_id_foreign');

            });
        Schema::dropIfExists('items');
    }
}
