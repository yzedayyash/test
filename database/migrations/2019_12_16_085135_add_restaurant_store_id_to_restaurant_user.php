<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRestaurantStoreIdToRestaurantUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('restaurant_users', function (Blueprint $table) {
            $table->unsignedInteger('restaurant_store_id');

            $table->foreign('restaurant_store_id')
            ->references('id')
            ->on('restaurant_stores');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('restaurant_users', function (Blueprint $table) {
            $table->dropForeign('restaurant_users_restaurant_store_id_foreign');

        });
    }
}
