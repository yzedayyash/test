<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCorporateItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('corporate_item', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('corporate_id');
            $table->unsignedInteger('item_id');
            $table->string('day');
            $table->foreign('corporate_id')
            ->references('id')
            ->on('corporates');
            $table->foreign('item_id')
            ->references('id')
            ->on('items');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('corporate_item', function(Blueprint $table){
            $table->dropForeign('corporate_item_corporate_id_foreign');
            $table->dropForeign('corporate_item_item_id_foreign');

            });
        Schema::dropIfExists('corporate_item');
    }
}
