<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCorporateCategorySettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('corporate_category_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->time('order_time_from');
            $table->time('order_time_to');
            $table->time('delivery_time_from');
            $table->time('delivery_time_to');

            $table->integer('created_at')->default(time());
            $table->integer('created_by')->nullable()->default(null);
            $table->integer('modified_at')->nullable()->default(null);
            $table->integer('modified_by')->nullable()->default(null);
            $table->integer('deleted')->nullable()->default(null);
            $table->integer('deleted_by')->nullable()->default(null);
            $table->integer('deleted_at')->nullable()->default(null);
            $table->integer('status')->default(0);

            $table->unsignedInteger('corporate_id');
            $table->foreign('corporate_id')
                ->references('id')
                ->on('corporates');

                $table->unsignedInteger('category_id');
                $table->foreign('category_id')
                    ->references('id')
                    ->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('corporate_category_settings', function(Blueprint $table){
            $table->dropForeign('corporate_category_settings_category_id_foreign');
            $table->dropForeign('corporate_category_settings_corporate_id_foreign');

            });
        Schema::dropIfExists('corporate_category_settings');
    }
}
