<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCorporateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('corporate_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('note')->nuallable()->default(null);
            $table->integer('created_at')->default(time());

            $table->float('price' , 8 , 2);



            $table->unsignedInteger('corporate_user_id');

            $table->foreign('corporate_user_id')
            ->references('id')
            ->on('corporate_users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('corporate_orders', function(Blueprint $table){
            $table->dropForeign('corporate_orders_corporate_user_id_foreign');

        });
        Schema::dropIfExists('corporate_orders');
    }
}
