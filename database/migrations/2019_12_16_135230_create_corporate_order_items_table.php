<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCorporateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('corporate_order_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('created_at')->default(time());


            $table->integer('quantity');

            $table->unsignedInteger('corporate_order_id');

            $table->foreign('corporate_order_id')
            ->references('id')
            ->on('corporate_orders');

            $table->unsignedInteger('item_id');

            $table->foreign('item_id')
            ->references('id')
            ->on('items');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('corporate_order_items', function(Blueprint $table){
            $table->dropForeign('corporate_order_items_corporate_order_id_foreign');
            $table->dropForeign('corporate_order_items_item_id_foreign');

        });
        Schema::dropIfExists('corporate_order_items');
    }
}
