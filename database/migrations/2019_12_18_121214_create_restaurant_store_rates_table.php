<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestaurantStoreRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_store_rates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rate');

            $table->unsignedInteger('restaurant_store_id');

            $table->foreign('restaurant_store_id')
            ->references('id')
            ->on('restaurant_stores');

            $table->unsignedInteger('corporate_user_id');

            $table->foreign('corporate_user_id')
            ->references('id')
            ->on('corporate_users');

            $table->integer('created_at')->default(time());

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('restaurant_store_rates', function(Blueprint $table){
            $table->dropForeign('restaurant_store_rates_restaurant_store_id_foreign');
            $table->dropForeign('restaurant_store_rates_corporate_user_id_foreign');

            });
        Schema::dropIfExists('restaurant_store_rates');
    }
}
