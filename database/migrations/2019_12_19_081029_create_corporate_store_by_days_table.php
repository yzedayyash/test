<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCorporateStoreByDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('corporate_store_by_days', function (Blueprint $table) {
            $table->increments('id');
            $table->string('day');
            $table->unsignedInteger('corporate_id');
            $table->unsignedInteger('restaurant_store_id');

            $table->foreign('corporate_id')
            ->references('id')
            ->on('corporates');
            $table->foreign('restaurant_store_id')
            ->references('id')
            ->on('restaurant_stores');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('restaurant_store_rates', function(Blueprint $table){
            $table->dropForeign('corporate_store_by_days_restaurant_store_id_foreign');
            $table->dropForeign('corporate_store_by_days_corporate_id_foreign');

            });
        Schema::dropIfExists('corporate_store_by_days');
    }
}
