<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemSpecificationTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_specification_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('language_id');
            $table->unsignedInteger('item_specification_id');

            $table->foreign('item_specification_id')
            ->references('id')
            ->on('item_specifications');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_specification_translations', function(Blueprint $table){
            $table->dropForeign('item_specification_translations_item_specification_id_foreign');
            });
        Schema::dropIfExists('item_specification_translations');
    }
}
