<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ItemItemSpecifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_item_specification', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('item_id');
            $table->unsignedInteger('item_specification_id');

            $table->foreign('item_id')
            ->references('id')
            ->on('items');
            $table->foreign('item_specification_id')
            ->references('id')
            ->on('item_specifications');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_item_specification', function(Blueprint $table){
            $table->dropForeign('item_item_specification_item_id_foreign');
            $table->dropForeign('item_item_specification_item_specification_id_foreign');

            });
        Schema::dropIfExists('item_item_specification');
    }
}
