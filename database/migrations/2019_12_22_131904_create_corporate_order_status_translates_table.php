<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCorporateOrderStatusTranslatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('corporate_order_status_translates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('language_id');
            $table->unsignedInteger('corporate_order_status_id');

            $table->foreign('corporate_order_status_id' , 'cost_status_id_foreign')
            ->references('id')
            ->on('corporate_order_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('corporate_order_status_translates', function(Blueprint $table){
            $table->dropForeign('cost_status_id_foreign');
            });
        Schema::dropIfExists('corporate_order_status_translates');
    }
}
