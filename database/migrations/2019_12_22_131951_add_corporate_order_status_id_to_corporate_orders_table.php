<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCorporateOrderStatusIdToCorporateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('corporate_orders', function (Blueprint $table) {
            $table->unsignedInteger('corporate_order_status_id');

            $table->foreign('corporate_order_status_id')
            ->references('id')
            ->on('corporate_order_statuses');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('corporate_orders', function (Blueprint $table) {
            $table->dropColumn('corporate_order_status_id');
            $table->dropForeign('corporate_orders_corporate_order_status_id_foreign');


        });
    }
}
