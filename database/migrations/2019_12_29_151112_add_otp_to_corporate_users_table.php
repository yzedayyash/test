<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOtpToCorporateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('corporate_users', function (Blueprint $table) {
            $table->string('otp', 100)->nullable();

$table->tinyInteger('is_validated')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('corporate_users', function (Blueprint $table) {
            $table->dropColumn('otp');
            $table->dropColumn('is_validated');
        });
    }
}
