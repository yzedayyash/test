<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRestaurantStoreIdToCorporateBasketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('corporate_baskets', function (Blueprint $table) {
            $table->unsignedInteger('restaurant_store_id');

        });

        Schema::table('corporate_orders', function (Blueprint $table) {
            $table->unsignedInteger('restaurant_store_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('corporate_baskets', function (Blueprint $table) {
            $table->dropColumn('restaurant_store_id');

        });

        Schema::table('corporate_orders', function (Blueprint $table) {
            $table->dropColumn('restaurant_store_id');

        });
    }
}
