<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddParentIdToCorporateDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('corporate_departments', function (Blueprint $table) {
            $table->unsignedInteger('parent_id')->nullable();
        });

        Schema::create('corporate_department_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('language_id');
            $table->unsignedInteger('corporate_department_id');
            $table->string('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('corporate_departments', function (Blueprint $table) {
            $table->dropColumn('parent_id');

        });
        Schema::dropIfExists('corporate_department_translations');

    }
}
