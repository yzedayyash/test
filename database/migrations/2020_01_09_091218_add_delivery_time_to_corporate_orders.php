<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDeliveryTimeToCorporateOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('corporate_orders', function (Blueprint $table) {
            $table->time('delivery_time_from');
            $table->time('delivery_time_to');
            $table->unsignedInteger('corporate_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('corporate_orders', function (Blueprint $table) {
            $table->dropColumn('delivery_time_from');
            $table->dropColumn('delivery_time_to');
            $table->dropColumn('corporate_id');

        });
    }
}
