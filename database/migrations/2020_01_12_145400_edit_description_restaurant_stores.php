<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditDescriptionRestaurantStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('restaurant_stores', function (Blueprint $table) {

            $table->dropColumn('description');
        });

        Schema::table('restaurant_store_translations', function (Blueprint $table) {

            $table->string('description');
                });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('restaurant_stores', function (Blueprint $table) {
            $table->string('description');
        });

        Schema::table('restaurant_store_translations', function (Blueprint $table) {

            $table->dropColumn('description');
                });
    }
}
