<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCorporateDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('corporate_devices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('device_type');
            $table->string('player_id');
            $table->unsignedInteger('corporate_user_id')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {


        Schema::table('corporate_devices', function(Blueprint $table){
            $table->dropForeign('corporate_devices_corporate_user_id_foreign');
            });
        Schema::dropIfExists('corporate_devices');
    }
}
