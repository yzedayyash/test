<?php

use App\Models\Item\Category;
use App\Models\Item\CategoryTranslation;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'slug' => 'dinner',

        ];

        Category::create($data);

        $translate_data = [
            'name' => 'dinner',
            'language_id' => 1,
            'category_id' => 1,

        ];

        CategoryTranslation::create($translate_data);

        $translate_data = [
            'name' => 'dinner ar',
            'language_id' => 2,
            'category_id' => 1,

        ];

        CategoryTranslation::create($translate_data);
    }
}
