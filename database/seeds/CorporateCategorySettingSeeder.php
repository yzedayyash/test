<?php

use Illuminate\Database\Seeder;
use App\Models\Corporate\CorporateCategorySetting;

class CorporateCategorySettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = [
            'order_time_from' => '12:00:00',
            'order_time_to' => '12:00:00',
            'delivery_time_from' => '12:00:00',
            'delivery_time_to' => '12:00:00',
            'corporate_id' => 1,
            'category_id' => 1,

        ];

        CorporateCategorySetting::create($data);
    }
}
