<?php

use Illuminate\Database\Seeder;
use App\Models\Corporate\Corporate;
use App\Models\Corporate\CorporateUser;
use Spatie\Permission\Models\Role;

class CorporateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $corporate =
            [
                'code' => 'corporate1',
                'primary_email' => 'corporate@corporate.com',
                'secondary_email' => 'corporate2@corporate.com',
                'pobox' => '123',
                'website' => 'administrator.com',
                'logofid' => 'Admin',
                'phone' => '1515151515',
                'company_name' => 'corporate',

                'tender_number_type' => '1',
                'registration_type' => '1',
                'description' => 'administrator@gmail.com',
                'address' => 'secret',
                'created_at' => time(),
                'created_by' => 1,
                'modified_at' => time(),
                'status' => 1
            ];


        Corporate::create($corporate);


        $corporate_user = [
                'name' => 'corporate1',
                'job_title' => '1111',
                'nationality' => '2222',
                'email' => 'corporateuser@user.com',
                'password' => bcrypt('password'),
                'salt' => 'Admin',
                'password_expire_date' => time(),
                'created_at' => time(),
                'modified_at' => time(),
                'created_by' => '1',
                'status' => 1,
                'active_role' => 5,
                'corporate_id' =>1,
        ];

        CorporateUser::create($corporate_user);


            $roles = [
               'dataentry',
                'editor',
               'manager',
               'employee',
            ];
            foreach ($roles as $role) {
                Role::firstOrCreate([
                    'guard_name' => 'web',
                    'name' => $role
                ]);
            };

            $users = [
                [
                    'job_title' => '1111',
                    'nationality' => '2222',
                    'salt' => 'Admin',
                    'password_expire_date' => time(),
                    'created_at' => time(),
                    'modified_at' => time(),
                    'created_by' => '1',
                    'status' => 1,
                    'active_role' => 5,
                    'corporate_id' =>1,
                    'role' => 'employee',
                    'name' => 'employee',
                    'email' => 'employee@gmail.com',
                    'password' => 'secret',
                ],
                [
                    'job_title' => '1111',
                    'nationality' => '2222',
                    'salt' => 'Admin',
                    'password_expire_date' => time(),
                    'created_at' => time(),
                    'modified_at' => time(),
                    'created_by' => '1',
                    'status' => 1,
                    'active_role' => 5,
                    'corporate_id' =>1,
                    'role' => 'dataentry',
                    'name' => 'dataentry',
                    'email' => 'data_entry@gmail.com',
                    'password' => 'secret',
                ],
                [
                    'job_title' => '1111',
                    'nationality' => '2222',
                    'salt' => 'Admin',
                    'password_expire_date' => time(),
                    'created_at' => time(),
                    'modified_at' => time(),
                    'created_by' => '1',
                    'status' => 1,
                    'active_role' => 5,
                    'corporate_id' =>1,
                    'role' => 'editor',
                    'name' => 'editor',
                    'email' => 'editor@gmail.com',
                    'password' => 'secret',
                ],
                [
                    'job_title' => '1111',
                    'nationality' => '2222',
                    'salt' => 'Admin',
                    'password_expire_date' => time(),
                    'created_at' => time(),
                    'modified_at' => time(),
                    'created_by' => '1',
                    'status' => 1,
                    'active_role' => 5,
                    'corporate_id' =>1,
                    'role' => 'manager',
                    'name' => 'Manager',
                    'email' => 'manager@gmail.com',
                    'password' => 'secret',
                ],
            ];

            foreach ($users as $user) {
                $exist = CorporateUser::where('email', $user['email'])->first();
                if(empty($exist)){
                    $userInsert = CorporateUser::firstOrCreate([
                        'name' => $user['name'],
                        'email' => $user['email'],
                        'password' => bcrypt($user['password']),
                        'job_title' => $user['job_title'],
                        'nationality' => $user['nationality'],
                        'created_by' => $user['created_by'],
                        'password_expire_date' => $user['password_expire_date'],
                        'salt' => $user['salt'],
                        'status' => $user['status'],

                        'active_role' => $user['active_role'],
                        'corporate_id' => $user['corporate_id'],

                    ]);
                    $userInsert->assignRole($user['role']);
                }
            }

    }
}
