<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminSeeder::class);
      //  $this->call(UserSeeder::class);

      $this->call(RestaurantDetailSeeder::class);
      $this->call(RestaurantStoreSeeder::class);

        $this->call(RestaurantSeeder::class);
        $this->call(CorporateSeeder::class);
        $this->call(CategorySeeder::class);

        $this->call(ItemUnitSeeder::class);
        $this->call(ItemSeeder::class);

        $this->call(CorporateCategorySettingSeeder::class);



    }
}
