<?php

use Illuminate\Database\Seeder;
use App\Models\Item\Item;
use App\Models\Item\ItemTranslation;
use App\Models\Item\ItemImage;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = [
            'slug' => 'item',
            'price' => 1,
            'old_price' => 12,
            'quantity_per_day' => 50,
            'category_id' => 1,
            'item_unit_id' => 1,
            'restaurant_store_id' => 1
        ];

        Item::create($data);

        $translate_data = [
            'name' => 'item',
            'description' => 'item desc',
            'item_id' => 1,
            'language_id' =>1

        ];

        ItemTranslation::create($translate_data);

        $translate_data = [
            'name' => 'item ar',
            'description' => 'item desc',
            'item_id' => 1,
            'language_id' =>2

        ];

        ItemTranslation::create($translate_data);


        $image_data = [
            'image' => 'image',
            'item_id' => 1
        ];

        ItemImage::create($image_data);

    }

    }

