<?php

use Illuminate\Database\Seeder;
use App\Models\Item\ItemUnit;
use App\Models\Item\ItemUnitTranslation;
class ItemUnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

            $data = [
                'slug' => 'kg',


        ];

        ItemUnit::create($data);

        $translate_data = [
            'name' => 'kg',
            'language_id' => 1,
            'item_unit_id' => 1,

    ];

    ItemUnitTranslation::create($translate_data);

    $translate_data = [
        'name' => 'kg ar',
        'language_id' => 2,
        'item_unit_id' => 1,

    ];

    ItemUnitTranslation::create($translate_data);

    }
}
