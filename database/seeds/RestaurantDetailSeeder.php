<?php

use Illuminate\Database\Seeder;
use App\Models\Restaurant\RestaurantDetail;

class RestaurantDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'name' => 'detail',
            'logo' => 'logo',
            'phone' => 'phone',
            'email' => 'details@user.com',
    ];

    RestaurantDetail::create($data);
    }
}
