<?php

use App\Models\Restaurant\RestaurantUser;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RestaurantSeeder extends Seeder
{
    public function run()
    {
        $roles = [
           'rest_admin',
            'rest_data',
           'rest_manager',
           'delivey_boy',
        ];
        foreach ($roles as $role) {
            Role::firstOrCreate([
                'guard_name' => 'restaurant',
                'name' => $role
            ]);
        };

        $admins = [
            [
                'role' => 'rest_admin',
                'name' => 'Admin',
                'email' => 'administrator@gmail.com',
                'password' => 'secret',
            ],
            [
                'role' => 'rest_data',
                'name' => 'Moderator',
                'email' => 'moderator@gmail.com',
                'password' => 'secret',
            ],
            [
                'role' => 'rest_manager',
                'name' => 'Manager',
                'email' => 'manager@gmail.com',
                'password' => 'secret',
            ],
        ];

        foreach ($admins as $admin) {
            $exist = RestaurantUser::where('email', $admin['email'])->first();
            if(empty($exist)){
                $super_admin = RestaurantUser::firstOrCreate([
                    'name' => $admin['name'],
                    'email' => $admin['email'],
                    'password' => bcrypt($admin['password']),
                    'restaurant_store_id' => 1
                ]);
                $super_admin->assignRole($admin['role']);
            }
        }
    }
}
