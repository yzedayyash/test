<?php

use Illuminate\Database\Seeder;
use App\Models\Restaurant\RestaurantStore;
use App\Models\Restaurant\RestaurantStoreTranslation;

class RestaurantStoreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $data = [
            'slug' => 'store',
            'logo' => 'logo',
            'phone' => 'phone',
            'email' => 'store@user.com',
            'adderss' => 'address',
            'restaurant_detail_id' => 1

    ];

    RestaurantStore::create($data);

    $translate_data = [
        'name' => 'store',
        'language_id' => 1,
        'restaurant_store_id' => 1,

];

RestaurantStoreTranslation::create($translate_data);

$translate_data = [
    'name' => 'store ar',
    'language_id' => 2,
    'restaurant_store_id' => 1,

];

RestaurantStoreTranslation::create($translate_data);
    }
}
