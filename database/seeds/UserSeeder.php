<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
class UserSeeder extends Seeder
{
    public function run()
    {
        $roles = [
           'dataentry',
            'editor',
           'manager',
        ];
        foreach ($roles as $role) {
            Role::firstOrCreate([
                'guard_name' => 'web',
                'name' => $role
            ]);
        };

        $users = [
            [
                'role' => 'dataentry',
                'name' => 'dataentry',
                'email' => 'data_entry@gmail.com',
                'password' => 'secret',
            ],
            [
                'role' => 'editor',
                'name' => 'editor',
                'email' => 'editor@gmail.com',
                'password' => 'secret',
            ],
            [
                'role' => 'manager',
                'name' => 'Manager',
                'email' => 'manager@gmail.com',
                'password' => 'secret',
            ],
        ];

        foreach ($users as $user) {
            $exist = User::where('email', $user['email'])->first();
            if(empty($exist)){
                $userInsert = User::firstOrCreate([
                    'name' => $user['name'],
                    'email' => $user['email'],
                    'password' => bcrypt($user['password']),
                ]);
                $userInsert->assignRole($user['role']);
            }
        }
    }
}