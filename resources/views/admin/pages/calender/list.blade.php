@extends('restaurant.layouts.master')
@section('main-content')

<div class="row">
<div class="card-body">

    @foreach($days as $key => $day)
    <a href="{{ route('restaurant.calender.list' , ['selected_day' => $day]) }}" class="{{ $selected_day == $day ? 'active' : '' }} btn btn-outline-primary m-1" type="button">{{ \Carbon\Carbon::parse($day)->format('D') }}</a>
    @endforeach

</div>
</div>
<div class="row">
    <div class=" col-md-12">


        <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
            <div class="card-body">




        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Company</th>
                        <th scope="col">Category</th>


                    </tr>
                </thead>
                <tbody>
                    @foreach ($corporates as $corporate)

                    <tr>
                        <th scope="row">    {{ $corporate['company_name'] }} - {{ $corporate['address'] }}</th>

                        <td>
                            @foreach($corporate['categories'] as $key => $value)
                            <strong> {{ $value['name'] }} </strong>

                         Time From {{  $value['delivery']->order_time_from }}
                       -{{  $value['delivery']->order_time_to}}

                         Delivery From {{  $value['delivery']->delivery_time_from }}
                        - {{  $value['delivery']->delivery_time_to }}

                            <br>
                            @endforeach

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>
    </div>

</div>

@endsection
