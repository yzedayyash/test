@extends('restaurant.layouts.master')
@section('main-content')

<div class="row">
    <div class=" col-md-12">

                <div class="card-body">
                    <h4 class="card-title mb-3">Items </h4>

                    <ul class="nav nav-tabs" id="myTab" role="tablist">

                        @foreach ($data as $key => $item)
                        <li class="nav-item"><a class="nav-link"" id="home-basic-tab" data-toggle="tab" href="#{{ $item->slug }}"
                                role="tab" aria-controls="{{ $item->slug }}" aria-selected="true">{{ $item->slug }}</a></li>
                        @endforeach
                    </ul>


                    <div class="tab-content" id="myTabContent">
                        @foreach ($data as $item)


                        <div class="tab-pane fade" id="{{ $item->slug }}" role="tabpanel" aria-labelledby="home-basic-tab">

                            @foreach($item->item as $key => $product)





                            <div class="card-body col-md-4">

                                <div class="d-flex flex-column flex-sm-row align-items-sm-center mb-3"><img class="avatar-lg mb-3 mb-sm-0 rounded mr-sm-3" src="{{asset($product->thumb)}}" alt="">
                                    <div class="flex-grow-1">
                                        <h5><a href="">{{ $product->name }}</a></h5>

                                        <p class="text-small text-danger m-0"> {{  $product->price  }} Qar
                                            <p class="text-small text-danger m-0"> Quatity
                                                <input type="text" id="quntity" data-item_id="{{ $product->id }}"  value=" {{ $product->quantity_per_day }}" />
                                                <button id="changeQuantity" data-item_id="{{ $product->id }}" class="btn small btn-primary">save</button>
                                            </p>
                                            <div>
                                                <label class="switch pr-5 switch-success mr-3"><span>Is Active</span>

                                                    {{-- <input type="checkbox" name="vehicle1" id="check" value="Bike"> I have a bike<br> --}}
                                                    <input type="checkbox" value="{{ $product->id }}" id="check"  @if( $product->status == 1) checked="checked" @endif ><span class="slider"></span>
                                                </label>
                                            </div>
                                    </div>



                                </div>



                            </div>

                            @endforeach

                        </div>



                    @endforeach
                  </div>
                </div>

    </div>

</div>

@endsection
@section('page-js')
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>



@endsection

@section('bottom-js')


    <script src="{{asset('assets/js/form.basic.script.js')}}"></script>
    <script>
    $(document).on('change', '#check', function() {
        if(this.checked) {
            var checked = 1;
        }else{
            var checked = 0;
        }


        $.ajax({
            url: '{{url("restaurant/deactiveItemAjax")}}',
            data:{checked: checked,item_id: this.value},

            type: "POST",
            headers: {
                'X-CSRF-Token': '{{ csrf_token() }}',
            },
            success: function(data){
            },
            error: function(){
                  alert("failure From php side!!! ");
             }

            });

        });

</script>

<script>
    $(document).on('click', '#changeQuantity', function() {
        var item_id = $(this).data('item_id');
        var quntity=         $("#quntity[data-item_id='" + item_id +"']").val();

        $.ajax({
            url: '{{url("restaurant/updateItemQuantityAjax")}}',
            data:{quantity: quntity,item_id: item_id},

            type: "POST",
            headers: {
                'X-CSRF-Token': '{{ csrf_token() }}',
            },
            success: function(data){

            },
            error: function(){
                  alert("failure From php side!!! ");
             }

            });

        });

</script>
@endsection
