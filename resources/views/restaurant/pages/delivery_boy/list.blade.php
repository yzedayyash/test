@extends('restaurant.layouts.master')
@section('main-content')

<div class="row">
    <div class="card-body">

        @foreach($days as $key => $day)
        <a href="{{ route('restaurant.delivery_boy.list' , ['selected_day' => $day]) }}"
            class="{{ $selected_day == $day ? 'active' : '' }} btn btn-outline-primary m-1"
            type="button">{{ \Carbon\Carbon::parse($day)->format('D') }}</a>
        @endforeach

    </div>
</div>
<div class="row">
    <div class=" col-md-5">


        <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
            <div class="card-body">




                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Company</th>
                                <th scope="col">Count Orders</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($corporates as $corporate)

                            <tr>
                                <th scope="row"> {{ $corporate['company_name'] }} - {{ $corporate['address'] }}</th>

                                <td>
                                    @foreach($corporate['categories'] as $key => $value)
                                    delivery {{  $value['delivery']->order_time_from }} {{ $value['name'] }}
                                    ({{ $value['count'] }}) Orders
                                    <br>
                                    @endforeach

                                </td>
                                <td><a class="text-success mr-2" id="assignOrder"
                                        data-corporate_id="{{ $corporate['id'] }}">assign</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>

    <div class=" col-md-5">
        <div class="card ">

            <div class="card-body">

                <ul class="nav nav-tabs" id="dropdwonTab">
                    <li class="nav-item dropdown"><a class="nav-link dropdown-toggle active" data-toggle="dropdown"
                            href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            Orders
                        </a>
                        <div class="dropdown-menu" x-placement="bottom-start"
                            style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 55px, 0px);">
                            <a class="dropdown-item show active" id="dropdown1-tab" href="#dropdown1" data-toggle="tab"
                                aria-controls="dropdown1" aria-expanded="true">Assigned</a><a class="dropdown-item show"
                                id="dropdown2-tab" href="#dropdown2" data-toggle="tab" aria-controls="dropdown2"
                                aria-expanded="true">Completed</a></div>
                    </li>
                    <li class="nav-item"><a class="nav-link" id="about-tab" data-toggle="tab" href="#about"
                            aria-controls="about" aria-expanded="false">Drivers</a></li>
                </ul>
                <div class="tab-content px-1 pt-1" id="dropdwonTabContent">

                    <div class="tab-pane active show" id="dropdown1" role="tabpanel" aria-labelledby="dropdown1-tab"
                        aria-expanded="true">

                            @foreach ($assigned_orders as $assigned_order)
                                {{ $assigned_order->delivery_boy_data['name']}} <br>
                                {{ $assigned_order->company_name}} <br><br><br>

                            @endforeach
                    </div>
                    <div class="tab-pane" id="dropdown2" role="tabpanel" aria-labelledby="dropdown2-tab"
                        aria-expanded="false">
                        <p>
                            Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeneys organic
                            lomo retro fanny pack lo-fi farm-to-table readymade. Messenger bag gentrify pitchfork
                            tattooed craft beer, iphone skateboard locavore.

                        </p>
                    </div>
                    <div class="tab-pane" id="about" role="tabpanel" aria-labelledby="about-tab" aria-expanded="false">
                        <p>
                            <button class="btn btn-primary" type="button" data-toggle="modal"
                                data-target="#insert_delivery">Add Delivery</button>
                            <br>

                        </p>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Delivery</th>
                                        <th scope="col">Email</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($delivery_users as $key => $value)

                                    <tr>
                                        <td>
                                            {{  $value->name }}


                                        </td>
                                        <td>
                                            {{  $value->email }}
                                        </td>


                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-lg" id='insert_delivery' tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Insert Delivery</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    {!! Form::open([route('restaurant.delivery_boy.insert')]) !!}
                    <div class="row">
                        <div class="col-md-12 form-group mb-3">
                            <label for="firstName2"> Name</label>

                            {{   Form::text('name',  null , array('class' => 'form-control form-control-rounded' , 'placeholder'=> 'Enter your first name')) }}
                        </div>

                        <div class="col-md-12 form-group mb-3">
                            <label for="exampleInputEmail2">Email address</label>
                            {!! Form::text('email', null , array('class' => 'form-control form-control-rounded' ,
                            'placeholder'=> 'Enter Email')) !!}
                        </div>

                        <div class="col-md-12 form-group mb-3">
                            <label for="exampleInputEmail2">Password</label>
                            {!! Form::text('password', null , array('class' => 'form-control form-control-rounded' ,
                            'placeholder'=> 'Enter Password')) !!}
                        </div>




                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                    {{ Form::submit('Insert' , array('class' => 'btn btn-primary')) }}
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>


    <div class="modal fade " id='assignDelivery' tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Select Delivery</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                {!! Form::open(array('url' => route('restaurant.delivery_boy.assign'))) !!}
                <input type="hidden" id="corporate_id" name="corporate_id" value="">

               <input type="hidden" id="selected_day" name="selected_day" value="{{ $selected_day }}">
                <select class=" form-control form-control-rounded" name="delivery" >
@foreach ($delivery_users as $value)
<option value="{{ $value->id }}">{{ $value->email }}</option>

@endforeach
                  </select>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                {{ Form::submit('Assign' , array('class' => 'btn btn-primary')) }}
            </div>
            {!! Form::close() !!}

        </div>
    </div>
</div>


    @endsection
    @section('bottom-js')
<script>
    $(document).on('click', '#assignOrder', function() {
        var corporate_id = $(this).data('corporate_id');
        $('#corporate_id').val(corporate_id)
      $('#corporate_id').val()

        $('#assignDelivery').modal('show');
    });
</script>

@endsection
