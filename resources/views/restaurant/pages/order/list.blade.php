@extends('restaurant.layouts.master')
@section('main-content')

<div class="row">
<div class="card-body">

    @foreach($days as $key => $day)
    <a href="{{ route('restaurant.order.list' , ['selected_day' => $day]) }}" class="{{ $selected_day == $day ? 'active' : '' }} btn btn-outline-primary m-1" type="button">{{ \Carbon\Carbon::parse($day)->format('D') }}</a>
    @endforeach

</div>
</div>
<div class="row">
    <div class=" col-md-5">


        <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
            <div class="card-body">




        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Company</th>
                        <th scope="col">Count Orders</th>


                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($corporates as $corporate)

                    <tr>
                        <th scope="row">    {{ $corporate['company_name'] }} - {{ $corporate['address'] }}</th>

                        <td>
                            @foreach($corporate['categories'] as $key => $value)
                         delivery {{  $value['delivery']->order_time_from }}   {{ $value['name'] }}    ({{ $value['count'] }}) Orders
                            <br>
                            @endforeach

                        </td>
                        <td><a class="text-success mr-2" id="viewOrder" data-corporate_id="{{ $corporate['id'] }}" href="#"><i class="nav-icon i-Pen-2 font-weight-bold"></i></a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>
    </div>



    <div class="col-md-7">

        <div class="card-body">
            <h4 class="card-title mb-3">Items </h4>


            <ul class="nav nav-tabs" id="myTab" role="tablist">


            </ul>


            <div class="tab-content" id="myTabContent">



          </div>
        </div>
    </div>
</div>

@endsection

@section('bottom-js')

<script>
    $(document).on('click', '#viewOrder', function() {
        var corporate_id = $(this).data('corporate_id');
        var urlParams = new URLSearchParams(window.location.search);

        date = urlParams.get('selected_day'); // "edit"
        $("#myTab").empty();
        $("#myTabContent").empty();
        if(!date) {


         date = new Date();
        date = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate();  }
        $.ajax({
            url: '{{url("restaurant/getCorporateOrderAjax")}}',
            data:{corporate_id: corporate_id , date : date},

            type: "POST",
            headers: {
                'X-CSRF-Token': '{{ csrf_token() }}',
            },
            success: function(data){

                jQuery.each( data, function( m, val ) {

                    $( "#myTab"  ).append('<li class="nav-item"><a class="nav-link"" id="home-basic-tab" data-toggle="tab" href="#' + val.category + '" role="tab" aria-controls="'+val.category+'" aria-selected="true">'+val.category+'</a></li>');
                    $( "#myTabContent"  ).append('<div class="tab-pane fade" id="'+val.category+'" role="tabpanel" aria-labelledby="home-basic-tab">');
                    $( "#"+val.category  ).append('<div class="accordion" id="accordion'+val.category+'"> </div>');



                        jQuery.each( val.items, function( i, val2 ) {


                            var countItems =0;
                            $( '#accordion'+val.category  ).append('<div class="card ul-card__border-radius id='+val2.slug+'"><a class="text-default" data-toggle="collapse" href="#col'+val2.slug+val.category+'" aria-expanded="true"><div class="card-header"><h6 class="card-title mb-0"><img width="90px" style="margin: 10px;" src='+window.location.origin+'/'+val2.thumb+'>'+val2.name+' <span id="count_'+val2.slug+val.category+'"> </span></h6></div></a></div>');
                            $( '#accordion'+val.category  ).append('<div class="collapse" id="col'+val2.slug+val.category+'" data-parent="#accordion'+val.category+'" style=""></div>');
                            jQuery.each( val2.details, function( i, val3 ) {

                                if(val3.note == null){
                                    val3.note='';
                                }
                                $( '#col'+val2.slug+val.category  ).append(' <div style="border:1px solid;" class="card-body">  '+val3.quantity+' * '+val3.user+'<span style="margin: 0 57px;" >#'+val3.order_id+'</span>'+' <br> Notes <br> '+val3.note+' </div>  ');
                                countItems = countItems + val3.quantity

                            });
                            $( '#count_'+val2.slug+val.category ).append(countItems+' Items')

                        });
                        url = '{{url("restaurant/dispatchOrder")}}'+'/'+date+'/'+val.corporate_id+'/'+val.category_id;
                        Printurl = '{{url("restaurant/printOrder")}}'+'/'+date+'/'+val.corporate_id+'/'+val.category_id;

                        $( '#accordion'+val.category  ).append('<a class="btn btn-primary" href="'+Printurl+'" role="button">Print</a>');
                        $( '#accordion'+val.category  ).append('<a class="btn btn-danger m-4" href="'+url+'" role="button">Dispatch</a>');

                });

            },
            error: function(){
                  alert("failure From php side!!! ");
             }

            });
    });

</script>
@endsection
