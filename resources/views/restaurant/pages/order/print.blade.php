@extends('restaurant.layouts.master')
@section('main-content')
<div class="row">
    <div class="col-md-6">
        Corporate Details
        <div class="card-body">

         Name:   {{ $corporate->company_name }}
         <br>
         Phone:   {{ $corporate->phone }}

        </div>
         </div>


         <div class="col-md-6">
            Delivery Details
            <div class="card-body">

             Location:   {{ $corporate->address }}
             <br>
             Delivery:   {{ $corporate->corporate_setting[0]->delivery_time_from }} - {{ $corporate->corporate_setting[0]->delivery_time_to }}
             <br>
             Contact:   {{ $restaurant->phone }}

            </div>
             </div>

    </div>

<div class="row">
    <div class=" col-md-12">


        <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
            <div class="card-body">




        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Order</th>
                        <th scope="col">Item</th>
                        <th scope="col">Note</th>



                    </tr>
                </thead>
                <tbody>


                    @foreach ($results as $result)
                    <tr>

                        <td>
                        {{ $result->corporate_user->name }}
                        </td>

                        <td>
                            {{ $result->id }}
                            </td>



                                <td>

@foreach ($result->corporate_order_item as $item)
    {{ $item->item->slug }} x {{ $item->quantity }}
    <br>
@endforeach
                                    </td>

                                    <td>
                                        @foreach ($result->corporate_order_item as $item)
                                      {{ $item->note }}
                                        <br>
                                    @endforeach
                                        </td>

                         </tr>

                        @endforeach


                </tbody>
            </table>
        </div>

    </div>
</div>
    </div>

</div>

@endsection
