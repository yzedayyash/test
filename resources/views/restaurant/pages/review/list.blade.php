
@extends('restaurant.layouts.master')
@section('main-content')


<div class="row">
    <div class=" col-md-12">


        <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
            <div class="card-body">




        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Customer Name</th>
                        <th scope="col">Rate</th>
                        <th scope="col">Date</th>



                    </tr>
                </thead>
                <tbody>


                    @foreach ($data as $data)
                    <tr>

                        <td>
                        {{ $data->corporate_user->name }}
                        </td>

                        <td>
                            {{ $data->rate }}
                            </td>


                            <td>
                                {{  date('d-m-Y H:i' , $data->created_at) }}
                                </td>



                         </tr>

                        @endforeach


                </tbody>
            </table>
        </div>

    </div>
</div>
    </div>

</div>

@endsection
