
@extends('restaurant.layouts.master')
@section('main-content')
{!! Form::open([route('restaurant.setting.update')]) !!}
    <div class="row">
        <div class="col-md-12 form-group mb-3">
            <label for="firstName2"> Name</label>

            {{   Form::text('name',  $data->name , array('class' => 'form-control form-control-rounded' , 'placeholder'=> 'Enter your first name')) }}
        </div>

        <div class="col-md-12 form-group mb-3">
            <label for="exampleInputEmail2">Email address</label>
            {!! Form::text('email',  $data->email  , array('class' => 'form-control form-control-rounded' , 'placeholder'=> 'Enter Email')) !!}
        </div>




        <div class="col-md-12">
            {{ Form::submit('Update' , array('class' => 'btn btn-primary')) }}
        </div>
    </div>
    {!! Form::close() !!}

@endsection
