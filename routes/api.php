<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['assign.guard:api']], function () {

    Route::post('register', 'UserController@register');
    Route::post('login', 'UserController@authenticate');

    Route::post('forgetPassword', 'UserController@forgetPassword');
    Route::get('resetPassword/{token}', 'UserController@resetPassword')->name('reset.password');
    Route::post('resetPassword', 'UserController@resetPasswordFromApplication');
    Route::post('verify_user', 'UserController@verify_user');

    Route::post('inviteUser', 'UserController@inviteUser');

    Route::get('invite_employee/{token}', 'UserController@invite_employee')->name('employee.invited');
    Route::post('register_device', 'UserController@register_device');


});

Route::group(['middleware' => ['assign.guard:api', 'jwt.auth', 'check.user']], function () {
    Route::get('user', 'UserController@getAuthenticatedUser');

    /*
    |--------------------------------------------------------------------------
    | Dashboard Routes
    |--------------------------------------------------------------------------
     */
    Route::prefix('dashboard')->group(function () {
        Route::get('/', 'Dashboard\DashboardController@index');
        Route::get('/employees', 'Dashboard\EmployeeController@employees');
        Route::put('/block_employee/{id}', 'Dashboard\EmployeeController@blockEmployee');
        Route::put('/delete_employee/{id}', 'Dashboard\EmployeeController@deleteEmployee');
        Route::get('/users', 'Dashboard\UserController@users');
        Route::get('/users/{id}', 'Dashboard\UserController@editUser');
        Route::put('/users/{id}', 'Dashboard\UserController@updateUser');
        Route::get('/add_users', 'Dashboard\UserController@addUser');
        Route::post('/users', 'Dashboard\UserController@insertUser');
        Route::get('/restaurants', 'Dashboard\RestaurantController@restaurants');
        Route::get('/settings', 'Dashboard\SettingsController@index');
        Route::get('/departments', 'Dashboard\DepartmentController@index');
        Route::get('/departmet_users/{id}', 'Dashboard\DepartmentController@departmetUsers');
        Route::post('/departments', 'Dashboard\DepartmentController@addDepartment');
        Route::get('/edit_department/{id}', 'Dashboard\DepartmentController@editDepartment');
        Route::put('/departments/{id}', 'Dashboard\DepartmentController@updateDepartment');
        Route::get('/assign', 'Dashboard\UserController@assign'); //test

        Route::post('/inviteEmployee', 'Dashboard\EmployeeController@inviteEmployee');


    });

        /*
    |--------------------------------------------------------------------------
    | Employee Routes
    |--------------------------------------------------------------------------
     */

        Route::get('home', 'HomePageController@getHomeData');
        Route::get('items', 'ItemController@getItems');
        Route::post('basket', 'BasketController@store');
        Route::get('basket', 'BasketController@index');
        Route::put('basket/{corporateBasket}', 'BasketController@update');
        Route::delete('basket/{id}', 'BasketController@destroy');


        Route::get('orders', 'OrderController@index');
        Route::post('orders', 'OrderController@store');

        Route::post('updateProfile', 'UserController@updateProfile');
        Route::post('updatePassword', 'UserController@updatePassword');


   //     Route::get('/employees', 'Dashboard\EmployeeController@employees');



});
//  Route::get('orders', 'OrderController@teststore');
Route::get('complete_payment', 'OrderController@completePayment');
