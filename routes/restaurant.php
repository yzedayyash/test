<?php

Route::group([
    'namespace' => 'Auth',
], function () {
    // Authentication Routes...
    Route::get('login', 'LoginController@showLoginForm')->name('login_page');
    Route::post('login', 'LoginController@login')->name('login');
    Route::get('logout', 'LoginController@logout')->name('logout');
});

Route::group([
    'middleware' => [
        'auth:restaurant',
    ],
], function () {

    // for all admins
    Route::get('/', 'RestaurantController@index')->name('dashboard');
    Route::get('home', 'RestaurantController@index')->name('dashboard');
    Route::get('dashboard', 'RestaurantController@index')->name('dashboard');

    Route::get('delivery_boy', 'DeliveryBoyController@index')->name('delivery_boy.list');
    Route::post('delivery_boy', 'DeliveryBoyController@insert')->name('delivery_boy.insert');

    Route::post('assignOrder', 'DeliveryBoyController@assignOrder')->name('delivery_boy.assign');




    Route::get('orders', 'OrderController@index')->name('order.list');
    Route::post('getCorporateOrderAjax', 'OrderController@getCorporateOrderAjax');
    Route::get('dispatchOrder/{date}/{corporate_id}/{category_id}', 'OrderController@dispatchOrder')->name('order.dispatch');
    Route::get('printOrder/{date}/{corporate_id}/{category_id}', 'OrderController@PrintOrder');

    Route::get('calender', 'CalenderController@index')->name('calender.list');
    Route::get('menu', 'MenuController@index')->name('menu.list');
    Route::post('deactiveItemAjax', 'MenuController@deactiveItemAjax');
    Route::post('updateItemQuantityAjax', 'MenuController@updateItemQuantityAjax');

    Route::get('review', 'ReviewController@index')->name('review.list');


    Route::get('setting', 'SettingController@index')->name('setting.edit');
    Route::post('setting', 'SettingController@update')->name('setting.update');

});

Route::group(['middleware' => ['role:rest_admin']], function () {
    Route::get('dashboard2', 'RestaurantController@index')->name('dashboard2');

});







// Route::view('gull/dashboard/dashboard1', 'dashboard.dashboardv1')->name('dashboard_version_1');
// Route::view('gull/dashboard/dashboard2', 'dashboard.dashboardv2')->name('dashboard_version_2');
// Route::view('gull/dashboard/dashboard3', 'dashboard.dashboardv3')->name('dashboard_version_3');
// Route::view('gull/dashboard/dashboard4', 'dashboard.dashboardv4')->name('dashboard_version_4');

// // uiKits
// Route::view('gull/uikits/alerts', 'uiKits.alerts')->name('alerts');
// Route::view('gull/uikits/accordion', 'uiKits.accordion')->name('accordion');
// Route::view('gull/uikits/buttons', 'uiKits.buttons')->name('buttons');
// Route::view('gull/uikits/badges', 'uiKits.badges')->name('badges');
// Route::view('gull/uikits/bootstrap-tab', 'uiKits.bootstrap-tab')->name('bootstrap-tab');
// Route::view('gull/uikits/carousel', 'uiKits.carousel')->name('carousel');
// Route::view('gull/uikits/collapsible', 'uiKits.collapsible')->name('collapsible');
// Route::view('gull/uikits/lists', 'uiKits.lists')->name('lists');
// Route::view('gull/uikits/pagination', 'uiKits.pagination')->name('pagination');
// Route::view('gull/uikits/popover', 'uiKits.popover')->name('popover');
// Route::view('gull/uikits/progressbar', 'uiKits.progressbar')->name('progressbar');
// Route::view('gull/uikits/tables', 'uiKits.tables')->name('tables');
// Route::view('gull/uikits/tabs', 'uiKits.tabs')->name('tabs');
// Route::view('gull/uikits/tooltip', 'uiKits.tooltip')->name('tooltip');
// Route::view('gull/uikits/modals', 'uiKits.modals')->name('modals');
// Route::view('gull/uikits/NoUislider', 'uiKits.NoUislider')->name('NoUislider');
// Route::view('gull/uikits/cards', 'uiKits.cards')->name('cards');
// Route::view('gull/uikits/cards-metrics', 'uiKits.cards-metrics')->name('cards-metrics');
// Route::view('gull/uikits/typography', 'uiKits.typography')->name('typography');

// // extra kits
// Route::view('gull/extrakits/dropDown', 'extraKits.dropDown')->name('dropDown');
// Route::view('gull/extrakits/imageCroper', 'extraKits.imageCroper')->name('imageCroper');
// Route::view('gull/extrakits/loader', 'extraKits.loader')->name('loader');
// Route::view('gull/extrakits/laddaButton', 'extraKits.laddaButton')->name('laddaButton');
// Route::view('gull/extrakits/toastr', 'extraKits.toastr')->name('toastr');
// Route::view('gull/extrakits/sweetAlert', 'extraKits.sweetAlert')->name('sweetAlert');
// Route::view('gull/extrakits/tour', 'extraKits.tour')->name('tour');
// Route::view('gull/extrakits/upload', 'extraKits.upload')->name('upload');


// // Apps
// Route::view('gull/apps/invoice', 'apps.invoice')->name('invoice');
// Route::view('gull/apps/inbox', 'apps.inbox')->name('inbox');
// Route::view('gull/apps/chat', 'apps.chat')->name('chat');
// Route::view('gull/apps/calendar', 'apps.calendar')->name('calendar');
// Route::view('gull/apps/task-manager-list', 'apps.task-manager-list')->name('task-manager-list');
// Route::view('gull/apps/task-manager', 'apps.task-manager')->name('task-manager');
// Route::view('gull/apps/toDo', 'apps.toDo')->name('toDo');
// Route::view('gull/apps/ecommerce/products', 'apps.ecommerce.products')->name('ecommerce-products');
// Route::view('gull/apps/ecommerce/product-details', 'apps.ecommerce.product-details')->name('ecommerce-product-details');
// Route::view('gull/apps/ecommerce/cart', 'apps.ecommerce.cart')->name('ecommerce-cart');
// Route::view('gull/apps/ecommerce/checkout', 'apps.ecommerce.checkout')->name('ecommerce-checkout');


// Route::view('gull/apps/contacts/lists', 'apps.contacts.lists')->name('contacts-lists');
// Route::view('gull/apps/contacts/contact-details', 'apps.contacts.contact-details')->name('contact-details');
// Route::view('gull/apps/contacts/grid', 'apps.contacts.grid')->name('contacts-grid');
// Route::view('gull/apps/contacts/contact-list-table', 'apps.contacts.contact-list-table')->name('contact-list-table');

// // forms
// Route::view('gull/forms/basic-action-bar', 'forms.basic-action-bar')->name('basic-action-bar');
// Route::view('gull/forms/multi-column-forms', 'forms.multi-column-forms')->name('multi-column-forms');
// Route::view('gull/forms/smartWizard', 'forms.smartWizard')->name('smartWizard');
// Route::view('gull/forms/tagInput', 'forms.tagInput')->name('tagInput');
// Route::view('gull/forms/forms-basic', 'forms.forms-basic')->name('forms-basic');
// Route::view('gull/forms/form-layouts', 'forms.form-layouts')->name('form-layouts');
// Route::view('gull/forms/form-input-group', 'forms.form-input-group')->name('form-input-group');
// Route::view('gull/forms/form-validation', 'forms.form-validation')->name('form-validation');
// Route::view('gull/forms/form-editor', 'forms.form-editor')->name('form-editor');

// // Charts
// Route::view('gull/charts/echarts', 'charts.echarts')->name('echarts');
// Route::view('gull/charts/chartjs', 'charts.chartjs')->name('chartjs');
// Route::view('gull/charts/apexLineCharts', 'charts.apexLineCharts')->name('apexLineCharts');
// Route::view('gull/charts/apexAreaCharts', 'charts.apexAreaCharts')->name('apexAreaCharts');
// Route::view('gull/charts/apexBarCharts', 'charts.apexBarCharts')->name('apexBarCharts');
// Route::view('gull/charts/apexColumnCharts', 'charts.apexColumnCharts')->name('apexColumnCharts');
// Route::view('gull/charts/apexRadialBarCharts', 'charts.apexRadialBarCharts')->name('apexRadialBarCharts');
// Route::view('gull/charts/apexRadarCharts', 'charts.apexRadarCharts')->name('apexRadarCharts');
// Route::view('gull/charts/apexPieDonutCharts', 'charts.apexPieDonutCharts')->name('apexPieDonutCharts');
// Route::view('gull/charts/apexSparklineCharts', 'charts.apexSparklineCharts')->name('apexSparklineCharts');
// Route::view('gull/charts/apexScatterCharts', 'charts.apexScatterCharts')->name('apexScatterCharts');
// Route::view('gull/charts/apexBubbleCharts', 'charts.apexBubbleCharts')->name('apexBubbleCharts');
// Route::view('gull/charts/apexCandleStickCharts', 'charts.apexCandleStickCharts')->name('apexCandleStickCharts');
// Route::view('gull/charts/apexMixCharts', 'charts.apexMixCharts')->name('apexMixCharts');

// // datatables
// Route::view('gull/datatables/basic-tables', 'datatables.basic-tables')->name('basic-tables');

// // sessions
// Route::view('gull/sessions/signIn', 'sessions.signIn')->name('signIn');
// Route::view('gull/sessions/signUp', 'sessions.signUp')->name('signUp');
// Route::view('gull/sessions/forgot', 'sessions.forgot')->name('forgot');

// // widgets
// Route::view('gull/widgets/card', 'widgets.card')->name('widget-card');
// Route::view('gull/widgets/statistics', 'widgets.statistics')->name('widget-statistics');
// Route::view('gull/widgets/list', 'widgets.list')->name('widget-list');
// Route::view('gull/widgets/app', 'widgets.app')->name('widget-app');
// Route::view('gull/widgets/weather-app', 'widgets.weather-app')->name('widget-weather-app');

// // others
// Route::view('gull/others/notFound', 'others.notFound')->name('notFound');
// Route::view('gull/others/user-profile', 'others.user-profile')->name('user-profile');
// Route::view('gull/others/starter', 'starter')->name('starter');
// Route::view('gull/others/faq', 'others.faq')->name('faq');
// Route::view('gull/others/pricing-table', 'others.pricing-table')->name('pricing-table');
// Route::view('gull/others/search-result', 'others.search-result')->name('search-result');
