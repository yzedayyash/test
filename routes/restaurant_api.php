<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => ['assign.guard:restaurant' ]], function() {

Route::post('register', 'RestaurantController@register');
Route::post('login', 'RestaurantController@authenticate');
});
Route::group(['middleware' => ['assign.guard:restaurant','jwt.auth' , 'check.restaurant' ]], function() {
    Route::get('user', 'RestaurantController@getAuthenticatedUser');
    Route::get('getDeliveryRestaurants', 'DeliveryBoyController@getDeliveryRestaurants');
    Route::post('changeStatus', 'DeliveryBoyController@changeStatus');


});

